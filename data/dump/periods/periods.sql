--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: periods; Type: TABLE DATA; Schema: public; Owner: coins
--

COPY periods (id, title, start_year, end_year, is_active, created_at, updated_at) FROM stdin;
1	Петр I	1699	1725	1	2016-03-07 12:38:14	2016-03-07 12:38:14
2	Екатерина I	1725	1727	1	2016-03-07 15:20:28	2016-03-07 15:20:28
3	Петр II	1727	1729	1	2016-03-25 12:26:12	2016-03-25 12:26:12
4	Анна Иоанновна	1730	1740	1	2016-03-25 12:28:23	2016-03-25 12:28:23
5	Иоанн Антонович	1740	1741	1	2016-03-25 12:29:07	2016-03-25 12:29:07
6	Елизавета	1741	1762	1	2016-03-25 12:30:35	2016-03-25 12:30:35
7	Петр III	1762	1762	1	2016-03-25 12:35:31	2016-03-25 12:35:31
8	Екатерина II	1762	1796	1	2016-03-25 12:36:46	2016-03-25 12:36:46
9	Павел I	1796	1801	1	2016-03-25 12:42:04	2016-03-25 12:42:04
10	Александр I	1801	1825	1	2016-03-25 12:43:45	2016-03-25 12:43:45
11	Николай I	1826	1855	1	2016-03-25 12:44:29	2016-03-25 12:44:29
12	Александр II	1854	1881	1	2016-03-25 12:45:03	2016-03-25 12:45:03
13	Александр III	1881	1894	1	2016-03-25 12:45:32	2016-03-25 12:45:32
14	Николай II	1894	1917	1	2016-03-25 12:46:03	2016-03-25 12:46:03
15	СССР	1917	1991	1	2016-03-25 12:47:00	2016-03-25 12:47:00
16	Россия	1991	2016	1	2016-03-25 12:47:25	2016-03-25 12:47:25
\.


--
-- Name: periods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: coins
--

SELECT pg_catalog.setval('periods_id_seq', 16, true);


--
-- PostgreSQL database dump complete
--

