<?php

use App\Helpers\Html;

?>
<div class="row">
    <div class="col-md-6">
        <p>
            Аукцион <?php echo $lot->bargain->auction->title . ' - ' . $lot->bargain->title; ?><br />
            Завершен <?php echo date('d.m.Y', strtotime($lot->bargain->date_close)); ?>
        </p>
    </div>
    <div class="col-md-6">
        <p>
            Начальная цена: <?php echo $lot->start_price; ?> руб.<br />
            Окончательная цена: <?php echo $lot->end_price; ?> руб.<br />
            Сохранность: <?php echo $lot->safety; ?>
        </p>
    </div>
</div>

<div>
    <h3>Лот <?php echo $lot->number; ?></h3>
    <p>
        <?php echo $lot->description; ?>
    </p>
    <div>
        <?php echo Html::image($lot->front_img, 'ЛОТ - ' . $lot->number, ['width' => '100px']); ?>
        <?php echo Html::image($lot->back_img, 'ЛОТ - ' . $lot->number, ['width' => '100px']); ?>
    </div>
</div>