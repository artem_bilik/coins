<?php

use App\Helpers\Html;

?>
<h3>
    <?php echo $nominal->section->period->title . ' - ' . $nominal->section->title . ' - ' . $nominal->title; ?>
</h3>
<?php foreach($nominal->coins as $coin) { ?>
    <h5>
        <?php echo $coin->title; ?>
    </h5>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Фото</th>
                <th>Год</th>
                <th>Описание</th>
                <th>Гурт</th>
                <th>Металл</th>
                <th>Петров</th>
                <th>Ильин</th>
                <th>Биткин</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($coin->varieties as $variety) { ?>
                <tr>
                    <td>
                        <?php echo Html::image($variety->front_img, '', ['style' => 'width: 100px;']); ?>
                        <?php echo Html::image($variety->back_img, '', ['style' => 'width: 100px;']); ?>
                    </td>
                    <td><?php echo $variety->year; ?></td>
                    <td><?php echo $variety->description; ?></td>
                    <td><?php echo ($variety->herd) ? $variety->herd->sign : '-'; ?></td>
                    <td><?php echo $variety->metal . ' ' . $variety->sample; ?></td>
                    <td><?php echo $variety->Petrov_rating; ?></td>
                    <td><?php echo $variety->Ilyin_rating; ?></td>
                    <td><?php echo $variety->Bitkin_rating; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>