<?php /*foreach($periods as $period) { */?><!--
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
            <?php /*echo $period->title . '(' . $period->start_year . '-' . $period->end_year . ')'; */?>
            <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <?php /*foreach($period->sections as $section){ */?>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><?php /*echo $section->title; */?> </a></li>
                <li role="presentation" class="divider"></li>
            <?php /*} */?>
        </ul>
    </div>
--><?php /*} */?>
<ul>
    <?php foreach($periods as $period) { ?>
        <li class="open_catalog">
            <?php echo $period->title; ?>
            <ul style="display: none;">
                <?php foreach($period->sections as $section) { ?>
                    <li>
                        <?php echo $section->title; ?>
                        <ul style="display: none;">
                            <?php foreach ($section->nominals as $nominal) { ?>
                                <li><a href="<?php echo action('Catalog@nominal', ['id' => $nominal->id]); ?>"><?php echo $nominal->title; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
</ul>
<script>
    window.onload = function(){
        $('.open_catalog').click(function(e){

            $(this).find('ul').toggle();
            e.preventBubble();
        });

    }
</script>
