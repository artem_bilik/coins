<ul class="nav nav-justified">
    <?php foreach($menu as $key => $item) { ?>
        <li<?php echo ($item[2] ? ' class="active"' : '')?>><a href="<?php echo $item[1]; ?>"><?php echo $item[0]; ?></a></li>
    <?php } ?>
    <li class="menu_search">
        <input type="text" class="form-control" id="search_text" placeholder="Поиск">
        <span class="input-group-addon" id="search_submit">Найти</span>
        <div id="search_hints">
        </div>
    </li>
</ul>
<style>
    .menu_search {
        position: relative;
    }
    .menu_search span {
        display: inline;
    }
    .menu_search input {
        display: inline!important;
        width: 50%!important;
    }
    #search_hints{
        position: absolute;
        top: 100%;
        background: #FFFFFF;
        z-index: 1;
    }
    .search_hint{
        border-bottom: 1px solid black;
    }
    #search_submit{
        cursor: pointer;
    }
</style>
<script>
   $('#search_text').bind('input', function(){
       if($(this).val()){
           $.get( "/search/hints/" + $(this).val(), function( data ) {
               $('.search_hint').unbind('click');
               var result = JSON.parse(data);
               var html = '';
               for(var i in result){
                   html += '<h5 class="search_hint">' + result[i] + '</h5>';
               }
               $('#search_hints').html(html);
               $('.search_hint').bind('click', function(){
                   $('#search_text').val($(this).text());
               });
           });
       } else {
           $('#search_hints').html('');
       }
   });
    $('#search_submit').bind('click', function(){
        if($('#search_text').val()){
            window.location = '/search/' + $('#search_text').val();
        }
    });
</script>