
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title><?php echo Theme::get('title'); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/themes/default/assets/css/style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://code.jquery.com/jquery-2.0.0.min.js"></script>

</head>

<body>

<div class="container">

    <div class="masthead">
        <h3 class="text-muted">Project name</h3>
        <?php echo Theme::widget('Menu')->render(); ?>
    </div>

    <!-- Example row of columns -->
    <div class="row">
        <div class="col-lg-3">
            <?php echo Theme::widget('Catalog')->render(); ?>
        </div>
        <div class="col-lg-9">
            <?php echo Theme::content(); ?>
        </div>
    </div>

    <!-- Site footer -->
    <div class="footer">
        <p>&copy; Company 2014</p>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<script src="/themes/default/assets/js/bootstrap.min.js"></script>
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>