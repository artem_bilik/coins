<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo Theme::get('title'); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400" rel="stylesheet">
	<link rel="stylesheet" href="/themes/coins/assets/css/style.css">
</head>
<body>
	<header>
		<a href="/" class="logo"><img src="/themes/coins/assets/img/logo.png" alt="">
			<h1>МОНЕТНЫЙ ДВОР</h1>
			<p>нумизматический портал</p></a>
			<div class="adress">
				<p>Москва, м. Академическая, ул. Профсоюзная, 3</p>
				<p>Москва, м. Молодежная, ул. Ярцевская, 4 </p>
				<p>8 (985) 626-66-00, 8 (901) 425-55-55s</p>
			</div>
      <?php echo Theme::widget('Menu')->render(); ?>
		</header>
		<main>
			<aside>
				<nav class="block-text">
          <?php echo Theme::widget('Catalog')->render(); ?>
				</nav>
				<div class="block-text kalendar">
					<?php echo Theme::widget('Years')->render(); ?>
				</div>
			</aside>
			<section>
          <?php echo Theme::content(); ?>
			</section>
		</main>
		<footer>
			<nav class="menu">
				<div class="part1">
					<p>© 2008-2016 Монетный Двор</p>
					<a href="#">Главная</a>
					<a href="/auctions">Архив аукционов</a>
					<span>8 (985) 626-66-00</span>
				</div>
				<div class="part2">
					<a href="#">Используя сайт, Вы принимаете соглашение</a>
					<a href="/catalog">Католог монет</a>
					<span>8 (901) 425-55-55</span>
				</div>
				<a href="#" class="vk"><img src="/themes/coins/assets/img/vk.png" alt=""></a>
			</nav>
		</footer>
		<script src="/themes/coins/assets/js/jquery-2.2.2.min.js"></script>
		<script src="/themes/coins/assets/js/jquery.flexslider-min.js"></script>
		<script src="/themes/coins/assets/js/script.js"></script>
	</body>
	</html>