$(window).load(function() {
	$('.flexslider').flexslider({
		animation: "slide",
		nextText:"",
		prevText:""
	});
});
$(document).ready(function(){
	$(".left-menu>li>a").click(function(e){
		e.preventDefault();
		$(this).toggleClass("open");
	});
	$(".pod-menu>li>a").click(function(e){
		e.preventDefault();
		$(this).toggleClass("open");
	});
    
    /* -- */
    var heightFoto = $('.wrapp_coin .fotoBl .foto').height();
    var heightFotoImg = $('.wrapp_coin .fotoBl .foto img').height();
    
    /* -- */
    $('.auction_wrapp .variable').click(function(){
        $('.auction_wrapp .variable').removeClass('active');
        $(this).addClass('active');
        var srcBool = $(this).find('.imgS').attr('src');
        $('.auction_wrapp .bigFoto .ico img').attr('src',srcBool);
    });
    $('#period_years_select').change(showYears);
    showYears();
	$('#find_input').bind('input', function(){
        var text = $(this).val();
        if(!text) { return 0; }
		$.get( "/search/hints/" + text, function( data ) {
            data = jQuery.parseJSON(data);
            if(data['text'] != $('#find_input').val()) { return 0; }
            var list = data['list'];
			var html = '';
			for(var i in list){
				html += '<p class="founded_item" data-year="' + list[i]['year'] + '" data-id="' + list[i]['nominal_id'] + '">' + list[i]['title'] + '</p>';
			}
			$('#founded_monets').html(html);
			$('.founded_item').click(function(){
				$('#find_input').val($(this).text());
				$('#find_id').val($(this).data('id'));
				$('#find_year').val($(this).data('year'));
				$('#founded_monets').html('');
				$('#search_form').submit();
			});
		});
	});
	$('#search_in_menu').bind('input', function(){
	    var text = $(this).val();
        if(!text) { return 0; }
		$.get( "/search/hints/" + text, function( data ) {
			data = jQuery.parseJSON(data);
            if(data['text'] != $('#search_in_menu').val()) { return 0; }
            var list = data['list'];
			var html = '';
			for(var i in list){
				html += '<p class="founded_item" data-year="' + list[i]['year'] + '" data-id="' + list[i]['nominal_id'] + '">' + list[i]['title'] + '</p>';
			}
			$('#founded_in_menu').html(html);
			$('#founded_in_menu .founded_item').click(function(){
				window.location = '/search?text=' + $(this).text() + '&id=' + $(this).data('id') + '&year=' + $(this).data('year');
			});
		});
	});
    $('#search_in_menu_find').click(function(){
        window.location = '/search?text=' + $('#search_in_menu').val() + '&id=' + $(this).data('id') + '&year=' + $(this).data('year');
    });
    
});
function showYears(){
	var from = parseInt($('#period_years_select option:selected').data('from'));
	var to   = parseInt($('#period_years_select option:selected').data('to'));
	var period = parseInt($('#period_years_select option:selected').data('period'));
	var html = '';
	$('#period_years_list').html(html);
	if(from == to){
		cnt = 1;
	} else {
		cnt = Math.ceil((to - from) / 4);
	}
	for(var i = 1; i <= cnt; i++){
		html += "<tr>";
		var k = 1;
		for(;from <= to && k <= 4; from++, k++){
			html += "<td><a href='/year/" + from + "/" + period + "'>" + from + "</a></td>";
		}
		html += "</tr>";
	}
	$('#period_years_list').html(html);
};

