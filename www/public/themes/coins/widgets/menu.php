<nav class="topmenu">
	<ul>
		<?php foreach($menu as $item) { ?>
			<li><a href="<?php echo $item[1]; ?>" class="<?php echo ($item[2] ? "active" : ""); ?>"><?php echo $item[0]; ?></a></li>
		<?php } ?>
		<li class="search_block">
			<input id="search_in_menu" />
			<div id="search_in_menu_find">Поиск</div>
			<div id="founded_in_menu"></div>
		</li>
	</ul>
</nav>