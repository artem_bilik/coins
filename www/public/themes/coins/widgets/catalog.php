<ul class="left-menu">
	<?php foreach($periods as $period) { ?>
		<li><a href="#"><?php echo $period->title . " (" . $period->start_year . "-" . $period->end_year . ")"; ?></a>
			<ul class="pod-menu">
				<?php foreach($period->sections as $section) { ?>
                    <li>
                        <a href="#"><?php echo $section->title; ?></a>
                        <ul class="pod2-menu">
							<?php foreach ($section->nominals as $nominal) { ?>
                                <li><a href="<?php echo action('Catalog@nominal', ['id' => $nominal->id]); ?>"><?php echo $nominal->title; ?></a></li>
                            <?php } ?>
						</ul>
                    </li>
                <?php } ?>
            </ul>
        </li>    
    <?php } ?>
</ul>