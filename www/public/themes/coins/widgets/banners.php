<?php if(!empty($banners)) { ?>
    <div class="flexslider">
    	<ul class="slides">
		    <?php foreach($banners as $banner) { ?>
				<li>
					<img src="<?php echo (new App\Helpers\ImageCache($banner->img, 847, 392))->getWebPath(); ?>" alt="" />
				</li>
		    <?php } ?>
		</ul>
	</div>
<?php } ?>
