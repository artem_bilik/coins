<select id="period_years_select" name="period">
	<?php foreach($periods as $k => $period) { ?>
		<option
			value="<?php echo $period->id; ?>"
			data-period="<?php echo $period->id; ?>"
			data-from="<?php echo $period->start_year; ?>"
			data-to="<?php echo $period->end_year; ?>"
			<?php echo ($period->id == $period_id) ? 'selected' : ''; ?>
		>
			<?php echo $period->title; ?>
		</option>
    <?php } ?>
</select>
<table id="period_years_list">

</table>
