<div class="auction_wrapp">
    <h2>МОНЕТЫ РОССИИ</h2>
    <h2><?php echo $period->title . ' (' . $period->start_year . '-' . $period->end_year . ')'; ?></h2>
</div>
<?php foreach($period->sections as $section) { ?>
    <div class="wrapp_table">
        <table>
            <tr><th><?php echo $section->title; ?></th></tr>
            <?php foreach($section->nominals as $nominal) { ?>
                <tr>
                    <td><a href="<?php echo action('Catalog@nominal', ['id' => $nominal->id]); ?>"><?php echo $nominal->title; ?></a></td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php } ?>