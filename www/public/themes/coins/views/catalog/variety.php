<?php use App\Helpers\Html; ?>
<div class="wrapp_coin">
    <div class="fotoBl">
        <h2><?php echo $variety->nominal->title; ?> <?php echo $variety->year; ?> года.</h2>
        <h3><?php echo $variety->description; ?></h3>
        <div class="fotoWrapp">
            <div class="foto">
                <?php echo Html::image((new App\Helpers\ImageCache($variety->front_img, 248, 254))->getWebPath(), $variety->title); ?>
                <?php echo Html::image((new App\Helpers\ImageCache($variety->front_img, 248, 254))->getWebPath(), $variety->title); ?>
            </div>
        </div>
        <div class="opisBl">
            <div class="zag">Техническая информация</div>
            <div class="txt">
                <?php if($variety->metal){ ?>
                    <p>Металл: <?php echo $variety->metal; ?></p>
                <?php } ?> 
                <?php if($variety->weight){ ?>
                    <p>Вес: <?php echo $variety->weight; ?> гр.</p>
                <?php } ?> 
                <?php if($variety->diameter){ ?>
                    <p>Диаметр: <?php echo $variety->diameter; ?> мм</p>
                <?php } ?> 
            </div>
        </div>
        <div class="opisBl opisBl_mar">
            <div class="zag">Литература и редкость</div>
            <div class="txt">
                <p>Биткин #<?php echo $variety->bitkin; ?> <?php echo ($variety->bitkin_rarity ? '(' . $variety->bitkin_rarity . ')' : ''); ?></p>
                <?php if($variety->Petrov_rating != '-') { ?>
                    <p>Петров - <?php echo $variety->Petrov_rating; ?> руб.</p>
                <?php } ?>
                <?php if($variety->Ilyin_rating != '-') { ?>
                    <p>Ильин - <?php echo $variety->Ilyin_rating; ?> руб.</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="fixedPrice_wrapp">
    <h2>ЗАФИКСИРОВАННЫЕ ПРОДАЖИ И ЦЕНЫ НА АУКЦИОНАХ</h2>
    <table>
        <tr><th>Фото</th><th>Аукцион</th><th>Дата</th><th>Сохранность</th><th>Цена</th><th></th></tr>
        <?php foreach($variety->lots as $lot) { ?>
            <tr>
                <td style="width: 150px;">
                    <?php echo Html::image((new App\Helpers\ImageCache($lot->front_img, 70, 67))->getWebPath(), $variety->title); ?><?php echo Html::image((new App\Helpers\ImageCache($lot->back_img, 70, 67))->getWebPath(), $variety->title); ?>
                </td>
                <td><?php echo $lot->bargain->auction->title; ?></td>
                <td><?php echo date('d.m.Y', strtotime($lot->bargain->date_open)); ?></td>
                <td><?php echo $lot->safety_text; ?></td>
                <td><?php if($lot->price) echo str_replace(' ', '&nbsp;', $lot->price) . '&nbsp;' . $lot->currency->sign; ?></td>
                <td><a href="<?php echo action('Auctions@lot', ['id' => $lot->id]); ?>" class="button"></a></td>
            </tr>
        <?php } ?>
    </table>
</div>