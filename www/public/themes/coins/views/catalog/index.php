<?php
    use App\Helpers\Html;
?>
<div class="catalog_wrapp">
    <h2>КАТАЛОГ МОНЕТ РОССИИ</h2>
    <div class="wrapp_Ico">
        <?php foreach($periods as $period) { ?>
            <div class="ico">
                <p class="txt_3"><?php echo $period->title; ?> </p>
                <a href="<?php echo action('Catalog@period', ['id' => $period->id]); ?>"><?php echo Html::image((new App\Helpers\ImageCache($period->img, 72, 100))->getWebPath(), $period->title); ?></a>
            <a href="<?php echo action('Catalog@period', ['id' => $period->id]); ?>" class=""><?php echo $period->start_year . '-' . $period->end_year; ?></a>
        </div>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>