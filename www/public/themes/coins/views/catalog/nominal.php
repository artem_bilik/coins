<div class="auction_wrapp">
    <h2><?php echo $nominal->title; ?></h2>
    <div class="clear"></div>
    <table>
        <tr><th>Год</th><th>Описание</th><th>Гурт</th><th>Металл</th><th>Петров</th><th>Ильин</th><th>Биткин</th><th>Цены</th></tr>
        <?php foreach($nominal->varieties as $variety) { ?>
            <tr>
                <td><?php echo $variety->year; ?></td>
                <td><p class="opis"><?php echo $variety->description; ?></p></td>
                <td><p class="gurt"><?php echo $variety->herd ? $variety->herd : ""; ?></p></td>
                <td><?php echo $variety->metal; ?></td>
                <td><?php echo $variety->Petrov_rating; ?></td>
                <td><?php echo $variety->Ilyin_rating; ?></td>
                <td><?php echo $variety->bitkin . ($variety->bitkin_rarity ? '(' . $variety->bitkin_rarity . ')' : ''); ?></td>
                <td><a href="<?php echo action('Catalog@variety', ['id' => $variety->id]); ?>">Цены (<?php echo $variety->lots_cnt; ?>)</a></td>
            </tr>
        <?php } ?>
    </table>
</div>