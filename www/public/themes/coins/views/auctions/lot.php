<?php

	use App\Helpers\Html;

?>
<main>
    <div class="aboutCoin">
        <div class="firstBl">
            <p class="txt_1"><a href="<?php echo action('Auctions@bargain', ['id' => $lot->bargain->id]); ?>"><?php echo $lot->bargain->auction->title; ?> #<?php echo $lot->bargain->number; ?></a></p>
            <p class="txt_2">Завершен <?php echo date('d.m.Y', strtotime($lot->bargain->date_open)); ?></p>
        </div>
        <div class="secondBl">
            <p class="txt_3 txt_4">Оценка: <span><?php echo $lot->start_price . ' ' . $lot->currency->sign; ?></span></p>
            <p class="txt_3">Цена: <span><?php echo $lot->price . ' ' . $lot->currency->sign; ?></span></p>
            <p class="txt_3">Сохранность: <span><?php echo $lot->safety_text; ?></span></p>
        </div>
        <div class="clear"></div>
        <div class="fotoBl">
            <p class="txt_1">Лот:</p>
            <p class="txt_2"><?php echo $lot->description; ?></p>
            <?php echo Html::image((new App\Helpers\ImageCache($lot->front_img, 353, 348))->getWebPath(), $lot->title, ['class' => 'coinIco']); ?>
            <?php echo Html::image((new App\Helpers\ImageCache($lot->back_img, 353, 348))->getWebPath(), $lot->title, ['class' => 'coinIco']); ?>
        </div>
    </div>			
</main>