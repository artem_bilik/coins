<?php

use \App\Helpers\Html;

?>
<div class="wrapp_arh">
    <h2><?php echo $auction->title; ?></h2>
    <table>
        <?php foreach($auction->bargains as $bargain) { ?>
            <tr>
                <td><?php echo Html::image((new App\Helpers\ImageCache($bargain->img, 110, 150))->getWebPath(), $bargain->title); ?></td>
                <td><a href="<?php echo action('Auctions@bargain', ['id' => $bargain->id]); ?>"><?php echo $bargain->title; ?></a></td>
                <td><?php echo $bargain->place_country . '<br />' . $bargain->place_city; ?></td>
                <td><?php echo date('d.m.Y', strtotime($bargain->date_open)); ?></td>
            </tr>
        <?php } ?>
    </table>
</div>