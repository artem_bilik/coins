<div class="wrapp_arh">
    <h2>АРХИВ ЗАВЕРШЕННЫХ АУКЦИОНОВ</h2>
    <table>
        <?php foreach($countries as $country) { ?>
            <tr>
                <td><?php echo $country->title; ?></td>
                <td>
                    <?php foreach($country->auctions as $auction) { ?>
                        <a href="<?php echo action('Auctions@view', ['id' => $auction->id]); ?>"><?php echo $auction->title; ?></a><br />
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>