<?php
	use App\Helpers\Html;
	$current_page = $lots->currentPage();
	$last_page = $lots->lastPage();
?>
<div class="auction_wrapp">
	<?php echo Html::image((new App\Helpers\ImageCache($bargain->img, 110, 150))->getWebPath(), $bargain->title, ['class' => 'ico']); ?>
	<h2><?php echo $bargain->auction->title; ?> - <?php echo date("d.m.Y", strtotime($bargain->date_open)); ?></h2>
	<h3><?php echo $bargain->title; ?> (<?php echo $bargain->place_city . ", " . $bargain->place_country; ?>)</h3>
	<div class="clear"></div>
	<table>
	    <tr>
	    	<th>№ лота</th>
	    	<th>Фото</th>
	    	<th>Описание</th>
	    	<th>Сохранность</th>
	    	<th>Цена</th>
	    </tr>
	    <?php foreach($lots as $k => $lot) { ?>
	    	<tr>
		        <td><?php echo (($current_page - 1) * 15 + 1 + $k); ?></td>
		        <td>
		        	<a href="<?php echo action('Auctions@lot', ['id' => $lot->id]); ?>">
		        		<?php echo Html::image((new App\Helpers\ImageCache($lot->front_img, 70, 70))->getWebPath(), $lot->title); ?>
		        		<?php echo Html::image((new App\Helpers\ImageCache($lot->back_img, 70, 70))->getWebPath(), $lot->title); ?>
		        	</a>
		        </td>
		        <td>
		        	<p class="opis"><?php echo $lot->description; ?></p>
		        </td>
		        <td><?php echo $lot->safety_text; ?></td>
		        <td>
		        	<?php echo ($lot->price) ? $lot->price . ' ' . $lot->currency->sign : 'Не продан'; ?>
		        </td>
		    </tr>
	    <?php } ?>
	</table>
	<div class="pageBl">
	    <p class="txt_2">Страницы:</p>
	    <?php for($page=1;$page <= $last_page;$page++) { ?>
	    	<div class="number<?php echo ($active_page == $page) ? ' active_page' : ''; ?>">
	    		<a href="<?php echo action('Auctions@bargain', ['id' => $bargain->id, 'page' => $page]); ?>"><?php echo $page; ?></a>
	    	</div>
	    <?php } ?> 
	</div>
</div>
