<h1>Страница входа</h1>
<?php if($error) { ?>
    <div class="alert alert-danger">
        <span><?php echo $error; ?></span>
    </div>
<?php } ?>
<?php echo $form->getHtml();