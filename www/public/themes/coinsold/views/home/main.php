<?php echo Theme::widget('Banners')->render(); ?>
<h1>Последние лоты</h1>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Изображение</th>
        <th>Название</th>
        <th>Дата</th>
        <th>Сохранность</th>
        <th>Стоимость</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($lots as $lot) { ?>
        <tr>
            <td>
                <img src="<?php echo (new App\Helpers\ImageCache($lot->front_img, 100, 100))->getWebPath(); ?>" />
                <img src="<?php echo (new App\Helpers\ImageCache($lot->back_img, 100, 100))->getWebPath(); ?>" />
            </td>
            <td><?php echo $lot->title; ?></td>
            <td><?php echo $lot->bargain->date_close; ?></td>
            <td><?php echo ($lot->safety) ? $lot->safety->title : ''; ?></td>
            <td><?php echo $lot->price; ?></td>
        </tr>
    <?php } ?>

    </tbody>
</table>