<?php
use App\Helpers\Html;
?>
<div>
    <div class="float: left;"><?php echo Html::image($bargain->img, $bargain->title); ?></div>
    <?php echo $bargain->auction->title; ?> - <?php echo date('d.m.Y', strtotime($bargain->date_close)); ?><br />
    <?php echo $bargain->title; ?> <?php echo $bargain->place_country; ?> <?php echo $bargain->place_city; ?>
</div>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>№</th>
            <th>Фото</th>
            <th>Описание</th>
            <th>Сохранность</th>
            <th>Цена</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($bargain->lots as $lot) { ?>
            <tr>
                <td><?php echo $lot->number; ?></td>
                <td>
                    <a href="<?php echo action('Auctions@lot', ['id' => $lot->id]); ?>"><?php echo Html::image($lot->front_img, 'ЛОТ - ' . $lot->number, ['width' => '100px']); ?></a>
                    <a href="<?php echo action('Auctions@lot', ['id' => $lot->id]); ?>"><?php echo Html::image($lot->back_img, 'ЛОТ - ' . $lot->number, ['width' => '100px']); ?></a>
                </td>
                <td><?php echo $lot->description; ?></td>
                <td><?php echo ($lot->safety ? $lot->safety->sign : ''); ?></td>
                <td><?php echo $lot->price; ?> руб.</td>
            </tr>
        <?php } ?>
    </tbody>
</table>