<?php

use \App\Helpers\Html;

?>
<h1><?php echo $auction->title; ?></h1>
<table class="table table-stripped table-bordered">
    <thead>
        <tr>
            <th>Логотип</th>
            <th>Название</th>
            <th>Место</th>
            <th>Дата начала</th>
            <th>Лоты</th>
            <th>Дата завершения</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($auction->bargains as $bargain) { ?>
            <tr>
                <td><?php echo Html::image($bargain->img, $bargain->title, ['width' => '100px']); ?></td>
                <td><a href="<?php echo action('Auctions@bargain', ['id' => $bargain->id]); ?>"><?php echo $bargain->title; ?></a></td>
                <td><?php echo $bargain->place_country . '<br />' . $bargain->place_city; ?></td>
                <td><?php echo date('d.m.Y', strtotime($bargain->date_open)); ?></td>
                <td><?php echo $bargain->lots_cnt; ?></td>
                <td><?php echo date('d.m.Y', strtotime($bargain->date_close)); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>