
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Sticky Footer Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/themes/coins/assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/themes/coins/assets/css/style.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="/themes/coins/assets/js/main.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top menu_top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a href="/"><img src="/themes/coins/assets/img/logo.png" /></a>
        </div>
        <div class="navbar-header">
          <h2 class="top_header">Кадашевский</h2>
          <h4 class="top_header">Цены монет на аукционах</h4>
        </div>
        <div class="collapse navbar-collapse pull-right">
          <ul class="nav navbar-nav top_menu_links">
            <li><a href="#">Каталог монет</a></li>
            <li><a href="#about">Список аукционных домов</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <!-- Begin page content -->
    <div class="container">
        <div class="row main_wrapper">
          <div class="col-md-4 left_column">
            <?php echo Theme::widget('Catalog')->render(); ?>
            <?php echo Theme::widget('Years')->render(); ?>
          </div>
          <div class="col-md-8 right_column">
            <div>
              <div class="form-group no_bottom_margin">
                <div class="input-group">
                  <div class="input-group-addon search_addon">...</div>
                  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Название лота">
                  <div class="input-group-addon search_addon">Поиск</div>
                </div>
              </div>
            </div>
            <?php echo Theme::content(); ?>
          </div>
        </div>
    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted color_blue pull-left">2016 © Кадашевский монетный двор</p>
        <p class="text-muted color_blue pull-right"><a>Каталог монет</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a>Список аукционных домов</a></p>
      </div>
    </div>
  </body>
</html>