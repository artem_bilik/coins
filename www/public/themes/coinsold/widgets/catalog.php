<ul class="catalog_menu">
    <?php foreach($periods as $period) { ?>
        <li class="catalog_opener">
            <span><?php echo $period->title . ' (' . $period->start_year . ' - ' . $period->end_year . ')'; ?></span>
            <ul class="catalog_second_menu" style="display: none;">
                <?php foreach($period->sections as $section) { ?>
                    <li class="catalog_opener">
                        <span><?php echo $section->title; ?></span>
                        <ul style="display: none;">
                            <?php foreach($section->nominals as $nominal) { ?>
                                <li><a href="<?php echo action('Catalog@nominal', ['id' => $nominal->id]); ?>"><?php echo $nominal->title; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
</ul>
