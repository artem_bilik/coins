<ul class="nav nav-justified">
    <?php foreach($menu as $key => $item) { ?>
        <li<?php echo ($item[2] ? ' class="active"' : '')?>><a href="<?php echo $item[1]; ?>"><?php echo $item[0]; ?></a></li>
    <?php } ?>
    <li class="menu_search">
        <input type="text" class="form-control" id="search_text" placeholder="Поиск">
        <span class="input-group-addon" id="search_submit">Найти</span>
        <div id="search_hints">
        </div>
    </li>
</ul>