<div class="years_wrapper">
    <select id="period_years" class="form-control">
        <?php foreach($periods as $period) { ?>
            <option
                value="<?php echo $period->id; ?>"
                data-from="<?php echo $period->start_year; ?>"
                data-to="<?php echo $period->end_year; ?>">
                <?php echo $period->title . ' (' . $period->start_year . ' - ' . $period->end_year . ')'; ?>
            </option>
        <?php } ?>
    </select>
    <div id="years_list">
        <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a> <a>1669</a>
    </div>
</div>

<script>
    function setYears(){
        var selected = $('#period_years').find('option:selected');
        var from = parseInt($(selected).data('from'));
        var to   = parseInt($(selected).data('to'));

        var html = '';

        if(from <= to){
            for(var i = from; i <= to; i++){
                html += '<a href="/year/' + i + '">' + i + '</a>';
            }
        }
        $('#years_list').html(html);

    };
    $('#period_years').bind('change', setYears);
    setYears();
</script>