<h1>
    <?php echo $model->getViewTitle(); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-danger" id="delete_model">Удалить</a>
    <?php foreach($model->getViewLinks() as $link) { ?>
        &nbsp;<a class="btn btn-info" href="<?php echo $link[0]; ?>"><?php echo $link[1]; ?></a>
    <?php } ?>
    <form action="/admin/<?php echo $model_name; ?>/delete" method="POST" id="delete_form">
        <input type="hidden" name="id" value="<?php echo $model->id; ?>" />
        <?php echo csrf_field(); ?>
    </form>
</h1>

<table class="table table-striped table-hover table-bordered">
    <tbody>
        <?php foreach($model->getFields() as $key => $field) { ?>
            <tr>
                <td><?php echo $model->getFieldTitle($field); ?></td>
                <td>
                    <div class="input-group">
                        <?php echo $model->getFieldUpdateAttribute($field, 'id_' . $key); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    window.onload = function(){
        $('.change-attribute').click(function(){
            var field = $(this).parent().find('input[name=fieldname]').val();
            var input = $(this).parent().find('[name=fieldvalue]');
            var input_type = $(input).attr('type')
            var value = '';

            var formData = new FormData();
            formData.append('_token', '<?php echo csrf_token(); ?>');
            formData.append('name', field);

            switch(input_type){
                case 'checkbox':
                    value = $(input).prop('checked') ? 1 : 0;
                    break;
                case 'file':
                    value = document.getElementById($(input).attr('id')).files[0];
                    break;
                default:
                    value = $(input).val();
            }
            var submitter = $(this);
            formData.append('value', value);
            $.ajax({
                url: "/admin/<?php echo $model_name; ?>/field_update/<?php echo $model->id; ?>",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                method: 'post',
                beforeSend: function(){
                    $(submitter).html('...');
                },
                error: function(){
                    errorMessage('При изменении произошла ошибка. Попробуйте еще раз.');
                    $(submitter).html('Изменить');
                },
                success: function(data, status){
                    if('error' == data.status){
                        errorMessage(data.error);
                    } else {
                        successMessage('Данные сохранены.');
                        location.reload();
                    }
                    $(submitter).html('Изменить');
                }

            });

        });
        $('#delete_model').click(function(){
            if(getConfirm('Удалить?')){
                $('#delete_form').submit();
            }
        });

    }
</script>