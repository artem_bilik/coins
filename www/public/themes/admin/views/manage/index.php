<div>
    <a class="btn btn-info" href="<?php echo $create_link; ?>">Добавить</a>
</div>
<form method="get">
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <?php foreach($model->getFieldsList() as $field) { ?>
                    <th><?php echo $field['title']; ?></th>
                <?php } ?>
                <th></th>
            </tr>
            <tr>
                <?php foreach($model->getFilters() as $field) { ?>
                    <th>
                        <?php echo $model->getFilterHtml($field); ?>
                    </th>
                <?php } ?>
                <th><input type="submit" value="Найти" /></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list as $item) { ?>
                <tr>
                    <?php foreach($model->getFieldsList() as $field) { ?>
                        <td><?php echo $item->getFieldValue($field); ?></td>
                    <?php } ?>
                    <td>
                        <a href="/admin/<?php echo $model_name; ?>/view/<?php echo $item->id; ?>" class="btn btn-default">Просмотр</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</form>
<?php echo $list->render(); ?>