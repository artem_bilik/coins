<h1>Лоты без разновидностей</h1>
<table class="table table-striped table-hover table-bordered">
    <tbody>
        <?php foreach($lots as $lot) { ?>
            <tr>
                <td><?php echo $lot->id; ?></td>
                <td><?php echo $lot->title; ?></td>
                <td><?php echo $lot->description; ?></td>
                <td><?php echo $lot->error; ?></td>
                <td><a href="/admin/lot/view/<?php echo $lot->id; ?>" class="btn btn-default" target="_blank">Просмотр</a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>