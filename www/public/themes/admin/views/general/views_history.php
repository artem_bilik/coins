<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>URL</th>
            <th>Кол-во просмотров</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($views as $view) { ?>
            <tr>
                <td><?php echo $view->url; ?></td>
                <td><?php echo $view->cnt; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>