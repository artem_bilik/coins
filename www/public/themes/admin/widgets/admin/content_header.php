<h1>
    <?php echo $title; ?>
    <small></small>
</h1>
<ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Админка</a></li>
    <?php foreach($breadcrumbs as $key => $breadcrumb) { ?>
        <?php
            if($last == $key) { ?>
                <li class="active"><?php echo $breadcrumb[0]; ?></li>
            <?php } else { ?>
                <li><a href="<?php echo $breadcrumb[1]; ?>"><i class="fa fa-dashboard"></i><?php echo $breadcrumb[0]; ?></a></li>
            <?php } ?>

    <?php } ?>
</ol>