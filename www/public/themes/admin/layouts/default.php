<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/themes/admin/assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/themes/admin/assets/css/admin.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/themes/admin/assets/css/skins.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/themes/admin/assets/css/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/themes/admin/assets/css/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/themes/admin/assets/css/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/themes/admin/assets/css/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/themes/admin/assets/css/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/themes/admin/assets/css/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="/themes/admin/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php echo Theme::widget('admin\Menu')->render(); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="padding: 20px; overflow: auto;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?php echo Theme::widget('admin\ContentHeader')->render(); ?>
        </section>

        <?php if(Session::has('success_message')) { ?>
            <div class="alert alert-success">
                <span><?php echo Session::get('success_message'); ?></span>
            </div>
        <?php } ?>
        <?php if(Session::has('error_message')) { ?>
            <div class="alert alert-danger">
                <span><?php echo Session::get('error_message'); ?></span>
            </div>
        <?php } ?>

        <!-- Main content -->
        <section class="content">
            <?php echo Theme::content(); ?>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2016 All rights reserved.
    </footer>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/themes/admin/assets/js/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="/themes/admin/assets/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/themes/admin/assets/js/morris.min.js"></script>
<!-- Sparkline -->
<script src="/themes/admin/assets/js/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/themes/admin/assets/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/themes/admin/assets/js/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/themes/admin/assets/js/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/themes/admin/assets/js/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/themes/admin/assets/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/themes/admin/assets/js/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/themes/admin/assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/themes/admin/assets/js/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="/themes/admin/assets/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/themes/admin/assets/js/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/themes/admin/assets/js/demo.js"></script>
<script src="/themes/admin/assets/js/main.js"></script>
</body>
</html>