<?php

namespace App\Http\Controllers;

use Request;
use Hash;
use Auth as AuthManager;
use Session;
use Cookie;

class Auth extends Controller
{

    public function login()
    {

        $login = new \App\Forms\Login($_POST);
        $error = '';
        if(Request::isMethod('post')){
            if($login->validate()){
                if($login->login()){
                    return redirect('/admin');
                } else {
                    $error = 'Такого пользователя не существует.';
                }
            }
        }


        return $this->getTheme()->scope('auth.login', ['form' => $login, 'error' => $error])->render();

    }

}