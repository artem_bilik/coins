<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\Bargain;
use App\Models\Country;
use App\Models\Lot;

class Auctions extends Controller
{

    public function index()
    {

        $countries = Country::with('auctions')->sort()->get();
        return $this->getTheme()->scope('auctions.index', [
            'countries' => $countries,
        ])->render();

    }

    public function view($id)
    {

        $auction = Auction::with('bargains')->active()->findOrFail($id);
        return $this->getTheme()->scope('auctions.view', [
            'auction' => $auction,
        ])->render();

    }

    public function bargain($id)
    {

        $bargain  = Bargain::findOrFail($id);
        $lots     = Lot::where('bargain_id', '=', $bargain->id)->paginate(15);
        if(isset($_GET['page'])){
            $active_page = $_GET['page'];
        } else {
            $active_page = 1;
        }
        return $this->getTheme()->scope('auctions.bargain', [
            'bargain' => $bargain,
            'lots' => $lots,
            'active_page' => $active_page,
        ])->render();

    }

    public function lot($id)
    {

        $lot = Lot::with('bargain.auction')->findOrFail($id);
        return $this->getTheme()->scope('auctions.lot', [
            'lot' => $lot,
        ])->render();

    }

}