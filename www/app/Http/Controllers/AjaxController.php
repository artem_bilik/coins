<?php

namespace App\Http\Controllers;

trait AjaxController
{

    public function getAjaxSuccess(array $data)
    {

        return response()->json(['status' => 'success', 'data' => $data]);

    }
    public function getAjaxError($error)
    {

        return response()->json(['status' => 'error', 'error' => $error]);

    }

}