<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use Theme;
use App\Models\ViewsHistory;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $_user = null;

    public function __construct()
    {

        $vh = new ViewsHistory();
        $vh->date = date('Y-m-d H:i:s');
        $vh->ip = $_SERVER['REMOTE_ADDR'];
        $vh->url = $_SERVER['REQUEST_URI'];
        $vh->save();
        $this->_user = Auth::user();

    }

    protected function getTheme($layout = 'default')
    {

        return Theme::uses(getenv('THEME'))->layout($layout);

    }

    protected function checkAccess(array $roles = [])
    {

        if(null === $this->_user || !in_array($this->_user->role, $roles)){
            App::abort(403, 'Недостаточно прав!');
        }

    }

}
