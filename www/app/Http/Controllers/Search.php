<?php

namespace App\Http\Controllers;

use App\Models\Nominal;
use App\Models\Variety;
use DB;

class Search extends Controller
{

    public function hints($text)
    {

        $list = [];

        $nominals = DB::table('nominals')->select('nominals.id', 'nominals.title', DB::raw("CONCAT(title, ' ', year, 'г.')"))
            ->join('varieties', 'varieties.nominal_id', '=', 'nominals.id')
            ->where( DB::raw("CONCAT(title, ' ', year, 'г.')"), 'like', $text . '%')
            ->distinct()
            ->get();

        foreach($nominals as $item){
            $years = [];
            foreach(Variety::where('nominal_id', '=', $item->id)->where( DB::raw("CONCAT('{$item->title}', ' ', year, 'г.')"), 'like', $text . '%')->select(['id', 'year'])->get() as $variety){
                if(!in_array($variety->year, $years)){
                    $years[] = $variety->year;
                    $list[] = [
                        'title' => $item->title . ' ' . $variety->year . 'г.',
                        'year' => $variety->year,
                        'nominal_id' => $item->id,

                    ];
                }
                if(count($list) > 15){
                    break;
                }
            }
        }
        return json_encode(['text' => $text, 'list' => $list]);

    }

    public function find()
    {

        $user_text = (isset($_GET['text'])) ? $_GET['text'] : null;
        if($user_text){
            $text = $this->getTextForFind($user_text);
        }

        if($text){
            $nominals = Nominal::active()->where('title', '=', $text['title'])->with(['varieties' => function($query) use ($text){
                $query->where('year', '=', $text['year']);
            }])->get();
            return $this->getTheme('default')->scope('search.find', ['text' => $user_text, 'year' => $text['year'], 'nominals' => $nominals])->render();
        }
        return $this->getTheme('default')->scope('search.find', ['text' => $user_text, 'year' => 0, 'nominals' => []])->render();


    }

    private function getTextForFind($text)
    {

        $matches = [];
        $res = preg_match_all('/([0-9]{4,4}\ ?г\.?)/', $text, $matches);
        if($res){
            $year = preg_replace('/[^0-9]/', '', $matches[0][0]);
            $title = trim(str_replace($matches[0][0], '', $text));
            return [
                'year'  => $year,
                'title' => $title,
            ];
        }


    }

}