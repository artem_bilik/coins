<?php

namespace App\Http\Controllers;


use App\Models\Nominal;
use App\Models\Period;
use App\Models\Variety;
use App\Widgets\Years;

class Catalog extends Controller
{

    public function index()
    {

        $periods = Period::active()->orderBy('id')->get();
        return $this->getTheme()->scope('catalog.index', ['periods' => $periods])->render();

    }

    public function period($id)
    {

        $period = Period::active()->findOrFail($id);
        return $this->getTheme()->scope('catalog.period', ['period' => $period])->render();

    }

    public function nominal($id)
    {

        $nominal = Nominal::active()->findOrFail($id);

        return $this->getTheme()->scope('catalog.nominal', ['nominal' => $nominal])->render();

    }
    public function variety($id)
    {

        $variety = Variety::findOrFail($id);
        return $this->getTheme()->scope('catalog.variety', ['variety' => $variety])->render();

    }
    public function year($year, $period)
    {

        $period = Period::active()->findOrFail($period);
        Years::setSelectedPeriod($period->id);
        return $this->getTheme()->scope('catalog.year', ['period' => $period, 'year' => $year])->render();
        
    }

}