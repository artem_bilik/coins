<?php

namespace App\Http\Controllers\Admin;
use Theme;
use Request;
use Session;
use App;
use App\Http\Controllers\AjaxController;

class Manage extends Base
{

    use AjaxController;

    public function index($model)
    {

        $model_name = $this->getModelName($model);
        $mc = new $model_name;
        foreach($mc->getBreadcrumbs() as $item){
            $this->addBreadcrumb($item[0], $item[1]);
        }
        $this->setPageTitle($mc->getListTitle());

        return $this->getTheme()->scope('manage.index', ['model_name' => $model, 'list' => $mc->search(), 'model' => $mc, 'create_link' => '/admin/' . $model . '/create'])->render();

    }

    public function create($model)
    {

        $model_name = $this->getModelName($model);

        $mc = new $model_name;
        foreach($_GET as $name => $param){
            $mc->$name = $param;
        }

        foreach($mc->getBreadcrumbs() as $item){
            $this->addBreadcrumb($item[0], $item[1]);
        }

        $this->addBreadcrumb('Новый');
        $this->setPageTitle($mc->getCreateTitle());

        if(!empty($_POST)){
            $mc->setData($_POST);
            if($mc->validate()){
                if($mc->createModel()){
                    Session::flash('success_message', $mc->getCreateTitle() . ' создан.');
                    return redirect('/admin/' . $model . '/view/' . $mc->id);
                } else {
                    Session::flash('error_message', 'При создании произошла ошибка. Попробуйте еще раз.');
                }
            }
        }

        return $this->getTheme()->scope('manage.create', ['model' => $mc])->render();

    }

    public function updateField($model, $id)
    {

        $model_name = $this->getModelName($model);
        $mc = $model_name::findOrFail($id);
        $name  = Request::input('name');
        $value = Request::input('value');
        $update_res = $mc->updateField($name, $value);
        if(true === $update_res){
            return $this->getAjaxSuccess(['name' => $name, 'value' => $value]);
        }

        if(false === $update_res){
            return App::abort(500);
        }

        return $this->getAjaxError($update_res);

    }

    public function delete($model)
    {

        $model_name = $this->getModelName($model);
        $id = Request::input('id');
        $mc = $model_name::findOrFail($id);
        if($mc->delete()){
            Session::flash('success_message', 'Запись удалена.');
            return redirect('/admin/' . $model . '/list');
        } else {
            Session::flash('error_message', 'При удалении произошла ошибка. Попробуйте еще раз.');
            return redirect('/admin/' . $model . '/view/' . $id);
        }

    }

    public function view($model, $id)
    {

        $model_name = $this->getModelName($model);
        $mc = $model_name::findOrFail($id);
        foreach($mc->getBreadcrumbs() as $item){
            $this->addBreadcrumb($item[0], $item[1]);
        }
        $this->addBreadcrumb('#'.$id, '/admin/' . $model . '/view/' . $id);

        return $this->getTheme()->scope('manage.view', ['model' => $mc, 'model_name' => $model])->render();

    }

    private function getModelName($model)
    {

        $model = '\\App\\Models\\' . ucfirst($model);
        if(!class_exists($model)){
            App::abort(404);
        }
        return $model;

    }

}