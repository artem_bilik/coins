<?php

namespace App\Http\Controllers\Admin;
use DB;

class General extends Base
{

    public function viewsHistory()
    {

        $views = DB::table('views_history')
            ->select(DB::raw('count(*) as cnt, url'))
            ->groupBy('url')
            ->orderBy('cnt', 'desc')
            ->get();

        return $this->getTheme()->scope('general.views_history', ['views' => $views])->render();

    }

}