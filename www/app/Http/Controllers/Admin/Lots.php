<?php
namespace App\Http\Controllers\Admin;


use App\Models\Lot;

class Lots extends Base
{

    public function error()
    {

        $lots = Lot::whereNull('variety_id')->orderBy('id', 'DESC')->get();
        return $this->getTheme()->scope('lots.error', ['lots' => $lots])->render();

    }

}