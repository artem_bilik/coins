<?php

namespace App\Http\Controllers\Admin;
use Theme;
use App\Widgets\Admin\ContentHeader;

class Base extends \App\Http\Controllers\Controller
{

    public function main()
    {

        return $this->getTheme()->scope('base.index')->render();

    }

    protected function getTheme($layout = 'default')
    {

        return Theme::uses('admin')->layout($layout);

    }

    protected function addBreadcrumb($label, $link = '#')
    {

        ContentHeader::addBreadcrumb($label, $link);

    }

    protected function setPageTitle($title)
    {

        ContentHeader::setPageTitle($title);

    }
}