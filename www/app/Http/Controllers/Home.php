<?php

namespace App\Http\Controllers;

class Home extends Controller
{

    public function main()
    {

        $lots = \App\Models\Lot::getLastLots();
        return $this->getTheme('home')->scope('home.main', ['lots' => $lots])->render();

    }

}