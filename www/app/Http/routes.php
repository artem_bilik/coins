<?php




Route::group(['middleware' => 'web'], function(){
    $get_routes = [
        ['/', 'Home@main'],
        ['/login', 'Auth@login'],
        ['/auctions', 'Auctions@index'],
        ['/auction/{id}', 'Auctions@view'],
        ['/bargain/{id}', 'Auctions@bargain'],
        ['/lot/{id}', 'Auctions@lot'],
        ['/nominal/{id}', 'Catalog@nominal'],
        ['/search/hints/{text}', 'Search@hints'],
        ['/search', 'Search@find'],
        ['/catalog', 'Catalog@index'],
        ['/period/{id}', 'Catalog@period'],
        ['/variety/{id}', 'Catalog@variety'],
        ['/year/{year}/{period}', 'Catalog@year'],
    ];

    $post_routes = [
        ['/login', 'Auth@login'],
    ];
    foreach($get_routes as $route){
        Route::get($route[0], $route[1]);
    }
    foreach($post_routes as $route){
        Route::post($route[0], $route[1]);
    }


    Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function(){
        Route::get('/admin', 'Base@main');
        Route::get('/admin/{model}/list', 'Manage@index');
        Route::get('/admin/{model}/view/{id}', 'Manage@view');
        Route::get('/admin/{model}/create', 'Manage@create');
        Route::post('/admin/{model}/create', 'Manage@create');
        Route::post('/admin/{model}/field_update/{id}', 'Manage@updateField');
        Route::post('/admin/{model}/delete', 'Manage@delete');
        Route::get('/admin/error-lots', 'Lots@error');
        Route::get('/admin/views-history', 'General@viewsHistory');
    });
});

