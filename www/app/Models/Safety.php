<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Safety extends Model implements IManage
{

    use Manage;

    private static $_list = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Сохранности',
            'view_title'  => function($country) {
                return "Сохранность - №" . $country->id;
            },
            'create_title' => 'Новая сохранность',
            'fields' => [
                [
                    'name'  => 'sign',
                    'title' => 'Код',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
            ],
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['sign'];
            }
        }

        return self::$_list;

    }
    public function getBreadcrumbs()
    {

        return [
            ['Сохранности', '/admin/safety/list']
        ];

    }

}
