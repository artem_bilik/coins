<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use App\Models\Manage\AttributesSetter;
use App\Helpers\Html;

class Variety extends Model implements IManage
{

    use Manage;
    use ModelWithFile;
    use AttributesSetter;

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Разновидности',
            'view_title'  => function($item) {
                return "Разновидность - №" . $item->id;
            },
            'create_title' => 'Новая разновидность',
            'fields' => [
                [
                    'name'  => 'id',
                    'title' => 'ID',
                    'list'  => true,
                    'field' => false,
                    'search_by' => '=',
                ],
                [
                    'name' => 'nominal_id',
                    'title' => 'Номинал',
                    'list' => true,
                    'field' => 'choice',
                    'choice_list' => Nominal::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Nominal::getList())),
                    'value' => function($item_id){
                        $items = Nominal::getList();
                        if(array_key_exists($item_id, $items)){
                            return $items[$item_id];
                        } return 'Номинал не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'year',
                    'title' => 'Год',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'description',
                    'title' => 'Описание',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => '',
                ],
                [
                    'name'  => 'metal',
                    'title' => 'Металл',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => '',
                ],
                [
                    'name'  => 'sample',
                    'title' => 'Проба',
                    'list'  => false,
                    'field' => 'text',
                    'validators' => '',
                ],
                [
                    'name'  => 'weight',
                    'title' => 'Вес (гр.)',
                    'list'  => false,
                    'field' => 'text',
                    'validators' => 'numeric',
                ],
                [
                    'name'  => 'diameter',
                    'title' => 'Диаметр (мм)',
                    'list'  => false,
                    'field' => 'number',
                    'validators' => 'integer',
                ],
                [
                    'name' => 'herd_id',
                    'title' => 'Гурт',
                    'list' => false,
                    'field' => 'choice',
                    'choice_list' => Herd::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Herd::getList())),
                    'value' => function($item_id){
                        $items = Herd::getList();
                        if(array_key_exists($item_id, $items)){
                            return $items[$item_id];
                        } return 'Гурт не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'Petrov_rating',
                    'title' => 'Петров',
                    'list'  => false,
                    'field' => 'text',
                    'validators' => 'integer',
                ],
                [
                    'name'  => 'Ilyin_rating',
                    'title' => 'Ильин',
                    'list'  => false,
                    'field' => 'text',
                    'validators' => 'integer',
                ],
                [
                    'name'  => 'bitkin',
                    'title' => 'Биткин',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => '',
                ],
                [
                    'name'  => 'front_img',
                    'title' => 'Вид спереди',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image($img, $this->title, ['width' => '100px']);
                    },
                ],
                [
                    'name'  => 'back_img',
                    'title' => 'Вид сзади',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image($img, $this->title, ['width' => '100px']);
                    },
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => false,
                    'validators' => 'integer',
                    'search_by'  => '=',
                ]
            ],
        ];

    }

    public function getBreadcrumbs()
    {
        $nominal = array_get($_GET, 'nominal_id');
        if(!$nominal){
            $nominal = ($this->nominal_id) ? $this->nominal_id : 0;
            $nominal_m = $this->nominal;
        } else {
            $nominal_m = Nominal::find($nominal);
        }


        if($nominal_m){
            $section = ($nominal_m->section_id) ? $nominal_m->section_id : 0;
            $period  = ($nominal_m->section) ? $nominal_m->section->period_id : 0;
        } else {
            $coin = $nominal = $section = $period = 0;
        }

        return [
            ['Периоды', '/admin/period/list'],
            ['Период #' . $period, '/admin/period/view/' . $period],
            ['Секции', '/admin/section/list?period_id=' . $period],
            ['Секция #' . $section, '/admin/section/view/' . $section],
            ['Номиналы', '/admin/nominal/list?section_id=' . $section],
            ['Номинал #' . $nominal, '/admin/nominal/view/' . $nominal],
            ['Разновидности', '/admin/variety/list?nominal_id=' . $nominal],
        ];

    }

    public function nominal()
    {

        return $this->belongsTo('\\App\\Models\\Nominal');

    }

    public function lots()
    {

        return $this->hasMany('\\App\\Models\\Lot')->orderBy('id', 'desc');;

    }

    public function getFileDir()
    {

        return 'periods/' . $this->nominal->section->period_id . '/sections/' . $this->nominal->section_id . '/nominals/' . $this->nominal_id . '/varieties/';

    }

    public function herd()
    {

        return $this->belongsTo('\\App\\Models\\Herd');

    }

    public function save(array $options = [])
    {

        if(!$this->description){
            $this->description = '-';
        }
        if(!$this->front_img){
            $this->front_img = 'error';
        }
        if(!$this->back_img){
            $this->back_img = 'error';
        }

        return parent::save($options);

    }
    // public function getFileDir()
    // {

    //     $variety = DB::table('varieties v')->select('v.nominal_id', 'n.section_id', 's.period_id')
    //         ->join('nominals n', 'n.id', '=', 'v.nominal_id')
    //         ->join('sections s', 's.id', '=', 'n.section_id')
    //         ->where('v.id', '=', $this->id)
    //         ->get();
    //     if(!$variety){
    //         throw new \Exception('Variety path to period not found!');
    //     }
    //     return 'periods/' . $variety['period_id'] . '/sections/' . $variety['section_id'] . '/nominals/' . $variety['nominal_id'] . '/varieties/';

    // }

}
