<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Nominal extends Model implements IManage
{

    use Manage;
    use AttributesSetter;


    protected $hidden = [
        'created_at', 'updated_at',
    ];

    private static $_list = [];
    private static $_periods_list = [];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Номиналы',
            'view_title'  => function($item) {
                return "Номинал - №" . $item->id;
            },
            'create_title' => 'Новый номинал',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'section_id',
                    'title' => 'Секция',
                    'list' => true,
                    'field' => 'choice',
                    'choice_list' => Section::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Section::getList())),
                    'value' => function($item_id){
                        $items = Section::getList();
                        if(array_key_exists($item_id, $items)){
                            return $items[$item_id];
                        } return 'Секция не установлена.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => true,
                    'validators' => 'integer',
                    'search_by'  => '=',
                ],
                [
                    'name'  => 'aliases',
                    'title' => 'Псевдонимы',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => '',
                ],
            ],
        ];

    }

    public function section()
    {

        return $this->belongsTo('\\App\Models\\Section');

    }

    public function coins()
    {

        return $this->hasMany('\\App\\Models\\Coin');

    }
    public function varieties()
    {

        return $this->hasMany('\\App\\Models\\Variety');

    }

    public function getViewLinks()
    {

        return [
            ['/admin/variety/list?nominal_id=' . $this->id, 'Список Разновидностей'],
            ['/admin/variety/create?nominal_id=' . $this->id, 'Добавить Разновидность']
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::with('section.period')->get() as $item){
                self::$_list[$item->id] = $item->section->period->title . ' - ' . $item->section->title . ' - ' . $item->title;
            }
        }

        return self::$_list;

    }

    public function getVarietiesByYear($year)
    {

        return Variety::where('nominal_id', '=', $this->id)->where('year', '=', $year)->get();

    }

    public function getBreadcrumbs()
    {

        $section = array_get($_GET, 'section_id');
        if(!$section){
            $section = ($this->section_id) ? $this->section_id : 0;
            $section_m = $this->section;
        } else {
            $section_m = Section::find($section);
        }

        if($section){
            $period = ($section_m->period_id) ? $section_m->period_id : 0;
        } else {
            $period  = 0;
            $section = 0;
        }
        return [
            ['Периоды', '/admin/period/list'],
            ['Период #' . $period, '/admin/period/view/' . $period],
            ['Секции', '/admin/section/list?period_id='.$period],
            ['Секция #' . $section, '/admin/section/view/' . $section],
            ['Номиналы', '/admin/nominal/list?section_id='.$section],
        ];

    }

    public static function getListByPeriod($period_id)
    {

        if(empty(self::$_periods_list)){
            foreach(Period::with('sections.nominals')->get() as $period){
                $nominals = [];
                foreach($period->sections as $section){
                    foreach($section->nominals as $nominal){
                        if($nominal->aliases){
                            $aliases = explode(',', $nominal->aliases);
                        } else {
                            $aliases = [];
                        }
                        array_unshift($aliases, $nominal->title);
                        $nominals[$nominal->id] = $aliases;
                    }
                }

                self::$_periods_list[$period->id] = $nominals;

            }
        }
        if(array_key_exists($period_id, self::$_periods_list)){
            return self::$_periods_list[$period_id];
        }
        return [];

    }
}
