<?php
namespace App\Models\Manage;

trait ModelWithFile
{

    public function saveUploadedFile(\Illuminate\Http\UploadedFile $file, $key)
    {

        $dir_name = $this->getFileDir();
        if(!$dir_name){
            throw new \Exception('Add _file_dir to ModelWithFile: ' . get_class($this));
        }
        $dir_name = 'images/' . $dir_name . '/';
        $dir = \App\Helpers\Directory::get(base_path($dir_name));
        $file_name = $key . '_' . $this->id . '.' . $file->extension();
        $file->move($dir->getPath(), $file_name);
        $this->$key = '/'.$dir_name . $file_name;

    }

    public function delete()
    {

        $img = base_path($this->img);
        if(is_file($img)){
            unlink($img);
        }
        return parent::delete();

    }

}