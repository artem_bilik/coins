<?php

namespace App\Models\Manage;

use App\Helpers\Html;
use Validator;
use Request;

trait Manage
{

    private $_errors = null;

    public function getFormHtml()
    {

        $fields_html = csrf_field();
        $is_file     = false;

        foreach($this->getFormFields() as $field){
            if('file' === array_get($field, 'field')){
                $is_file = true;
            }
            $fields_html .= $this->getFieldHtml($field);
        }
        $attributes = ['class' => 'form-horizontal', 'method' => 'post'];
        if($is_file){
            $attributes['enctype'] = 'multipart/form-data';
        }
        return Html::form($attributes,
            $fields_html . Html::formHorizontalSubmit('Сохранить'));

    }

    protected function getFieldHtml($field)
    {

        if(false === $field['field']){
            return '';
        }
        switch($field['field']){
            case 'text':
                $field_html = Html::formHorizontalText(
                    $field['name'],
                    $this->{$field['name']},
                    $field['title'],
                    $field['title'],
                    (null !== $this->_errors ? $this->_errors->get($field['name']) : [])
                );
                break;
            case 'choice':
                $field_html = Html::formHorizontalSelect(
                    $field['name'],
                    $this->{$field['name']},
                    $field['choice_list'],
                    $field['title'],
                    (null !== $this->_errors ? $this->_errors->get($field['name']) : [])
                );
                break;
            case 'checkbox':
                $field_html = Html::formHorizontalCheckbox($field['name'], $this->{$field['name']}, $field['title'], (null !== $this->_errors ? $this->_errors->get($field['name']) : []));
                break;
            case 'file':
                $field_html = Html::formHorizontalFile($field['name'], $field['title'], (null !== $this->_errors ? $this->_errors->get($field['name']) : []));
                break;
            case 'number':
                $field_html = Html::formHorizontalNumber(
                    $field['name'],
                    $this->{$field['name']},
                    $field['title'],
                    $field['title'],
                    (null !== $this->_errors ? $this->_errors->get($field['name']) : [])
                );
                break;
            case 'date':
                $field_html = Html::formHorizontalDate(
                    $field['name'],
                    $this->{$field['name']},
                    $field['title'],
                    $field['title'],
                    (null !== $this->_errors ? $this->_errors->get($field['name']) : [])
                );
                break;
            default:
                return $field['field'];
        }
        return $field_html;

    }

    protected function getFieldUpdateHtml($field, $id)
    {

        if(false === $field['field']){
            return '';
        }
        switch($field['field']){
            case 'text':
                $field_html = Html::input('text', 'fieldvalue', $this->{$field['name']}, ['class' => 'form-control', 'id' => $id])
                    . Html::input('hidden', 'fieldname', $field['name']) ;
                break;
            case 'choice':
                $field_html = Html::select('fieldvalue', $this->{$field['name']}, $field['choice_list'], ['class' => 'form-control', 'id' => $id])
                    . Html::input('hidden', 'fieldname', $field['name']) ;
                break;
            case 'checkbox':
                $attributes = [
                    'id' => $id
                ];
                if(1 == $this->{$field['name']}){
                    $attributes['checked'] = 'true';
                }
                $field_html = Html::input('checkbox', 'fieldvalue', '', $attributes)
                    . Html::input('hidden', 'fieldname', $field['name']) ;
                break;
            case 'file':
                $field_html = Html::image((new \App\Helpers\ImageCache($this->{$field['name']}, 100, 100))->getWebPath(), '', ['class' => 'field_file_preview'])
                    . Html::input('file', 'fieldvalue', '', ['id' => $id])
                    . Html::input('hidden', 'fieldname', $field['name'])
                ;
                break;
            case 'number':
                $field_html = Html::input('number', 'fieldvalue', $this->{$field['name']}, ['class' => 'form-control', 'id' => $id])
                    . Html::input('hidden', 'fieldname', $field['name']) ;
                break;
            case 'date':
                $field_html = Html::input('date', 'fieldvalue', $this->{$field['name']}, ['class' => 'form-control', 'id' => $id])
                    . Html::input('hidden', 'fieldname', $field['name']) ;
                break;
            default:
                return $field['field'];
        }
        return $field_html;

    }

    public function updateField($name, $field_value)
    {

        foreach($this->getFormFields() as $field){
            if($field['name'] === $name){
                if('file' === $field['field']){
                    $files = Request::file();
                    if(array_key_exists('value', $files)){
                        $field_value = $files['value'];
                    }
                }
                $validators = array_get($field, 'validators');
                if($validators){
                    $validator = Validator::make([$name => $field_value], [$name => $validators]);
                    if($validator->fails()){
                        $messages = $validator->messages();
                        return implode(', ' , $messages->get($name));
                    }

                }

                if('file' === $field['field']){
                    $this->saveUploadedFile($field_value, $field['name']);
                } else {
                    $this->setUpdateAttribute($name, $field_value);
                }

                return $this->save();
            }
        }

        return 'Этот аттрибут не подлежит изменению.';

    }

    public function setUpdateAttribute($name, $value)
    {

        $this->$name = $value;

    }

    public function getFieldUpdateAttribute($field_name, $id)
    {

        foreach($this->getFormFields() as $field){
            if($field['name'] === $field_name['name']){
                if(($field['field'] === false)){
                    return Html::input('text', '', $this->{$field['name']}, ['class' => 'form-control', 'id' => $id, 'disabled' => 'disabled']);
                }
                return $this->getFieldUpdateHtml($field, $id) . '<span class="input-group-addon change-attribute">Изменить</span>';
            }
        }

        return $this->getFieldValue($field_name);


    }

    public function validate()
    {

        foreach($this->getFormFields() as $field){
            if('file' === array_get($field, 'field')){
                $files = Request::file();
                if(array_key_exists($field['name'], $files)){
                    $this->{$field['name']} = $files[$field['name']];
                }
            }
        }

        $validators = $this->getValidators();

        $validator = Validator::make($validators['values'], $validators['validators'], $validators['messages']);
        if($validator->fails()){
            $this->_errors = $validator->messages();
            return false;
        }
        return true;

    }

    public function createModel(array $options = [])
    {

        $files = [];
        foreach($this->getFormFields() as $field){
            if('file' === array_get($field, 'field')){
                if($this->{$field['name']} instanceof \Illuminate\Http\UploadedFile){
                    $files[$field['name']] = $this->{$field['name']};
                }
                $this->{$field['name']} = 'error';
            }
        }

        if(empty($files)){
            return $this->save($options);
        }
        $this->save($options);

        foreach($files as $key => $file){
            $this->saveUploadedFile($file, $key);
        }
        return $this->save($options);

    }

    public function getValidators()
    {

        $validators = [
            'values'     => [],
            'validators' => [],
            'messages'   => [
                'required' => 'Это поле обязательно для заполнения.',
                'in'       => 'Выберите из предложенного списка'
            ]
        ];
        foreach($this->getFormFields() as $field){
            $validators['values'][$field['name']] = $this->{$field['name']};
            if(isset($field['validators'])){
                $validators['validators'][$field['name']] = $field['validators'];
            }
            if(isset($field['messages'])){
                $validators['messages'] = array_merge($validators['messages'], $field['messages']);

            }
        }
        return $validators;

    }

    public function setData(array $data)
    {

        foreach($this->getFormFields() as $field){
            if(array_key_exists($field['name'], $data)){
                $this->{$field['name']} = $data[$field['name']];
            }
        }

    }

    public function getFormFields()
    {

        $fields = [];
        foreach($this->getFields() as $field){
            if(isset($field['field'])){
                $fields[] = $field;
            }
        }

        return $fields;

    }
    public function getCreateTitle()
    {

        $title = $this->getValue('create_title');
        if(null === $title){
            return 'Новый ' . self::class;
        }
        return $title;

    }
    public function getViewTitle()
    {

        $title = $this->getValue('view_title');
        if(null === $title){
            return '#' . $this->getKey();
        }
        return $title;

    }
    public function getListTitle()
    {

        $title = $this->getValue('list_title');
        if(null === $title){
            return self::class;
        }
        return $title;

    }

    public function getFilters()
    {

        $request = request();
        $filters = $this->getFieldsList();
        foreach($filters as $key => $filter) {
            $filters[$key]['filter_value'] = $request->input($filter['name']);
        }

        return $filters;

    }

    public function getFilterHtml(array $field)
    {

        if(array_key_exists('filter', $field)){
            return $field['filter'];
        }
        switch($field['field']){
            case 'choice':
                return Html::select($field['name'], $field['filter_value'], ['' => ''] + $field['choice_list']);
            case 'checkbox':
                return Html::select($field['name'], $field['filter_value'], ['' => '', 1 => 'Да', 0 => 'Нет']);
            case 'number':
                return Html::input('number', $field['name'], $field['filter_value']);
            default:
                return Html::input('text', $field['name'], $field['filter_value']);
        }


    }

    public function getFieldsList()
    {

        $fields = $this->getFields();
        $result = [];
        foreach($fields as $field){
            if(isset($field['list']) && $field['list']){
                $result[] = $field;
            }
        }
        return $result;

    }

    public function getFields()
    {

        $fields = $this->getValue('fields');
        if(!$fields){
            return [];
        }
        return $fields;

    }

    public function getFieldTitle(array $field)
    {

        if(array_key_exists('title', $field)){
            return $field['title'];
        } else {
            return ucfirst($field['name']);
        }

    }
    public function getFieldValue(array $field)
    {

        if(array_key_exists('value', $field)){
            if(is_callable($field['value'])){
                return $field['value']($this->{$field['name']});
            }
            return $field['value'];
        }
        return $this->{$field['name']};

    }

    public function search()
    {

        $searcher = null;
        foreach($this->getFilters() as $field){
            if($field['filter_value'] === '' || $field['filter_value'] === null) { continue; }
            $params = $this->createParamsForWhere($field);
            if(null === $searcher) {
                $searcher = $this::where($params[0], $params[1], $params[2]);
            } else {
                $searcher = $searcher->where($params[0], $params[1], $params[2]);
            }
        }

        if(null === $searcher){
            return $this::orderBy('id', 'DESC')->paginate(200);
        }

        return $searcher->orderBy('id', 'DESC')->paginate(200);

    }

    private function createParamsForWhere(array $field)
    {

        $params = [
            0 => $field['name'],
            1 => 'like',
            2 => '%' . $field['filter_value'] . '%',
        ];
        if(isset($field['search_by'])){
            $params[1] = $field['search_by'];
            $params[2] = $field['filter_value'];
        }
        if('number' === $field['field']){
            $params[1] = '=';
            $params[2] = $field['filter_value'];
        }
        return $params;

    }

    private function getValue($value)
    {

        $map = $this->getManageMap();
        if(array_key_exists($value, $map)){
            if(is_callable($map[$value])){
                return $map[$value]($this);
            }
            return $map[$value];
        }
        return null;

    }

    public function getViewLinks()
    {

        return [];

    }

}