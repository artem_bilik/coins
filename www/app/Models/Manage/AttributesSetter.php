<?php
namespace App\Models\Manage;

trait AttributesSetter
{

    public function setIsActiveAttribute($is_active)
    {

        $this->attributes['is_active'] = (int)((boolean)$is_active);

    }
    public function setSortAttribute($sort)
    {

        $this->attributes['sort'] = (int)$sort;

    }

    public function scopeSort($query)
    {

        return $query->orderBy('sort', 'ASC');

    }

    public function scopeActive($query)
    {

        return $query->where('is_active', '1');

    }

}