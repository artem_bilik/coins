<?php

namespace App\Models;

use App\Helpers\Html;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use App\Models\Manage\AttributesSetter;

class Auction extends Model implements IManage
{

    use Manage;
    use ModelWithFile;
    use AttributesSetter;

    private static $_list = [];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Аукционы',
            'view_title'  => function($item) {
                return "Аукцион - №" . $item->id;
            },
            'create_title' => 'Новый аукцион',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'img',
                    'title' => 'Фото',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image((new \App\Helpers\ImageCache($img, 100, 100))->getWebPath(), $this->title, ['width' => '100px']);
                    },
                ],
                [
                    'name'  => 'country_id',
                    'title' => 'Страна',
                    'list'  => true,
                    'field' => 'choice',
                    'choice_list' => Country::getList(),
                    'validators'  => 'required|in:' . implode(',', array_keys(Country::getList())),
                    'value' => function($country_id){
                        $countries = Country::getList();
                        if(array_key_exists($country_id, $countries)){
                            return $countries[$country_id];
                        } return 'Страна не установлена.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => true,
                    'validators' => 'integer',
                    'search_by'  => '=',
                ]
            ],
        ];

    }


    public function getViewLinks()
    {

        return [
            ['/admin/bargain/list?auction_id=' . $this->id, 'Список Торгов'],
            ['/admin/bargain/create?auction_id=' . $this->id, 'Добавить Торг']
        ];

    }

    public function bargains()
    {

        return $this->hasMany('\\App\\Models\\Bargain');

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['title'];
            }
        }

        return self::$_list;

    }

    protected function getFileDir()
    {

        return 'auctions/' . $this->id;

    }
    public function getBreadcrumbs()
    {

        return [
            ['Аукционы', '/admin/auction/list']
        ];

    }


}
