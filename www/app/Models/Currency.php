<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Currency extends Model implements IManage
{

    use Manage;

    private static $_list = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Валюты',
            'view_title'  => function($country) {
                return "Валюта - №" . $country->id;
            },
            'create_title' => 'Новая валюта',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'sign',
                    'title' => 'Знак',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'abbr',
                    'title' => 'Сокращение',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
            ],
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['title'];
            }
        }

        return self::$_list;

    }
    public function getBreadcrumbs()
    {

        return [
            ['Валюты', '/admin/currency/list']
        ];

    }

}
