<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use App\Helpers\Html;

class Period extends Model implements IManage
{

    use Manage;
    use AttributesSetter;
    use ModelWithFile;

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    private static $_list = [];
    private static $_gnumber_list = [];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Периоды',
            'view_title'  => function($country) {
                return "Период - №" . $country->id;
            },
            'create_title' => 'Новый период',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'start_year',
                    'title' => 'Начало периода',
                    'list'  => true,
                    'field' => 'number',
                    'validators' => 'required|integer',
                    'search_by' => '=',
                ],
                [
                    'name'  => 'end_year',
                    'title' => 'Окончание периода',
                    'list'  => true,
                    'field' => 'number',
                    'validators' => 'required|integer',
                    'search_by' => '=',
                ],
                [
                    'name'  => 'img',
                    'title' => 'Изображение',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image((new \App\Helpers\ImageCache($img, 100, 100))->getWebPath(), $this->title, []);
                    },
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
            ],
        ];

    }

    public function getViewLinks()
    {

        return [
            ['/admin/section/list?period_id=' . $this->id, 'Список Секций'],
            ['/admin/section/create?period_id=' . $this->id, 'Добавить Секцию']
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['title'];
            }
        }

        return self::$_list;

    }

    public function sections()
    {

        return $this->hasMany('\\App\\Models\\Section');

    }

    public function getBreadcrumbs()
    {

        return [
            ['Периоды', '/admin/period/list']
        ];

    }

    public static function getPeriodIdByGNumber($gnumber)
    {

        if(empty(self::$_gnumber_list)){
            foreach(self::select(['id', 'gnumber'])->get() as $period){
                self::$_gnumber_list[$period->gnumber] = $period->id;
            }
        }
        if(array_key_exists($gnumber, self::$_gnumber_list)){
            return  self::$_gnumber_list[$gnumber];
        }
        return null;

    }
    public function getFileDir()
    {

        return 'periods/';

    }

    public function getSectionsWithVarietiesInYear($year)
    {

        $sections = Section::select('sections.id')->where('period_id', '=', $this->id)->join('nominals', 'nominals.section_id', '=', 'sections.id')
            ->join('varieties', 'varieties.nominal_id', '=', 'nominals.id')->where('varieties.year', '=', $year)->get();
        $section_ids = [];
        foreach($sections as $section){
            $section_ids[] = $section->id;
        }

        return Section::wherein('id', $section_ids)->active()->get();

    }

}
