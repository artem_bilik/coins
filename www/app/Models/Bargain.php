<?php

namespace App\Models;

use App\Helpers\Html;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use App\Models\Manage\AttributesSetter;

class Bargain extends Model implements IManage
{

    use Manage;
    use ModelWithFile;

    private static $_list = [];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Торги',
            'view_title'  => function($item) {
                return "Торги - №" . $item->id;
            },
            'create_title' => 'Новый торг',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'number',
                    'title' => 'Номер',
                    'list' => true,
                    'field' => 'number',
                    'validators' => 'required|integer',
                ],
                [
                    'name'  => 'auction_id',
                    'title' => 'Аукцион',
                    'list'  => true,
                    'field' => 'choice',
                    'filter' => '',
                    'choice_list' => Auction::getList(),
                    'validators'  => 'required|in:' . implode(',', array_keys(Auction::getList())),
                    'value' => function($auction_id){
                        $auctions = Auction::getList();
                        if(array_key_exists($auction_id, $auctions)){
                            return $auctions[$auction_id];
                        } return 'Аукцион не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'img',
                    'title' => 'Фото',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'required|image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image((new \App\Helpers\ImageCache($img, 100, 100))->getWebPath(), $this->title, []);
                    },
                ],
                [
                    'name'  => 'place_country',
                    'title' => 'Страна проведения',
                    'list'  => true,
                    'field' => 'text',
                ],
                [
                    'name'  => 'place_city',
                    'title' => 'Город проведения',
                    'list'  => true,
                    'field' => 'text',
                ],
                [
                    'name' => 'date_open',
                    'title' => 'Дата открытия',
                    'list' => true,
                    'field' => 'date',
                    'validators' => 'date',
                    'filter' => ''
                ],
                [
                    'name' => 'date_close',
                    'title' => 'Дата закрытия',
                    'list' => true,
                    'field' => 'date',
                    'validators' => 'date',
                    'filter' => ''
                ],
                [
                    'name' => 'lots_cnt',
                    'title' => 'Лоты',
                    'list' => true,
                    'field' => 'number',
                    'validators' => 'integer'
                ]
            ],
        ];

    }

    public function scopeClosed($query)
    {

        return $query->where('date_close', '<', date('Y-m-d'));

    }

    public function scopeSort($query)
    {

        return $query->orderBy('date_close', 'DESC');

    }

    public function auction()
    {

        return $this->belongsTo('\\App\\Models\\Auction');

    }

    public function lots()
    {

        return $this->hasMany('\\App\\Models\\Lot');

    }


    public static function getList()
    {

        if(!self::$_list){
            foreach(self::with('auction')->get() as $item){
                self::$_list[$item['id']] = $item->auction->title . ' - ' . $item->title;
            }
        }

        return self::$_list;

    }

    public function getFileDir()
    {

        return 'auctions/' . $this->auction_id . '/bargains/' . $this->id;

    }

    public function getViewLinks()
    {

        return [
            ['/admin/lot/list?bargain_id=' . $this->id, 'Список Лотов'],
            ['/admin/lot/create?bargain_id=' . $this->id, 'Добавить Лот']
        ];

    }
    public function getBreadcrumbs()
    {

        $auction = array_get($_GET, 'auction_id');
        if(!$auction){
            $auction = ($this->auction_id) ? $this->auction_id : 0;
        }
        return [
            ['Аукционы', '/admin/auction/list'],
            ['Аукцион #' . $auction, '/admin/auction/view/' . $auction],
            ['Торги', '/admin/bargain/list?auction_id=' . $auction]
        ];

    }


}
