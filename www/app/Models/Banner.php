<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;

class Banner extends Model implements IManage
{

    use Manage;
    use ModelWithFile;
    use AttributesSetter;

    public function getManageMap()
    {

        return [
            'list_title'  => 'Баннеры',
            'view_title'  => function($banner) {
                return "Баннер - №" . $banner->id;
            },
            'create_title' => 'Новый баннер',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'img',
                    'title' => 'Баннер',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'required|image',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],

                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'list'  => true,
                    'field' => 'checkbox',
                ],
                [
                    'name' => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => true,
                ]
            ],
        ];

    }

    public function save(array $options = [])
    {

        if(!$this->sort){
            $this->sort = 0;
        }
        return parent::save($options);

    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }
    public function getBreadcrumbs()
    {

        return [
            ['Баннеры', '/admin/banner/list']
        ];

    }
    public function getFileDir()
    {

        return 'banners';

    }

}
