<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Herd extends Model implements IManage
{

    use Manage;

    private static $_list = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Гурты',
            'view_title'  => function($item) {
                return "Гурт - №" . $item->id;
            },
            'create_title' => 'Новый гурт',
            'fields' => [
                [
                    'name'  => 'sign',
                    'title' => 'Обозначение',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
            ],
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['sign'];
            }
        }

        return self::$_list;

    }
    public function getBreadcrumbs()
    {

        return [
            ['Гурты', '/admin/herd/list']
        ];

    }

}
