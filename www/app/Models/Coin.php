<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Coin extends Model implements IManage
{

    use Manage;
    use AttributesSetter;

    private static $_list = [];


    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Разновидности',
            'view_title'  => function($item) {
                return "Разновидность - №" . $item->id;
            },
            'create_title' => 'Новая разновидность',
            'fields' => [
                [
                    'name'  => 'id',
                    'title' => 'ID',
                    'list'  => true,
                    'field' => false,
                    'search_by' => '=',
                ],
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'nominal_id',
                    'title' => 'Номинал',
                    'list' => true,
                    'field' => 'choice',
                    'choice_list' => Nominal::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Nominal::getList())),
                    'value' => function($item_id){
                        $items = Nominal::getList();
                        if(array_key_exists($item_id, $items)){
                            return $items[$item_id];
                        } return 'Номинал не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => true,
                    'validators' => 'integer',
                    'search_by'  => '=',
                ],
                [
                    'name'  => 'years',
                    'title' => 'Года',
                    'field' => 'text',
                    'list'  => false,
                    'validators' => 'required',
                ],
            ],
        ];

    }

    public function nominal()
    {

        return $this->belongsTo('\\App\Models\\Nominal');

    }

    public function getBreadcrumbs()
    {

        $nominal = array_get($_GET, 'nominal_id');
        if(!$nominal){
            $nominal = ($this->nominal_id) ? $this->nominal_id : 0;
            $nominal_m = $this->nominal;
        } else {
            $nominal_m = Nominal::with('section')->find($nominal);
        }
        if($nominal_m){
            $section = ($nominal_m->section) ? $nominal_m->section->id : 0;
            $period  = ($nominal_m->section) ? $nominal_m->section->period_id : 0;
        } else {
            $nominal = $section = $period = 0;
        }

        return [
            ['Периоды', '/admin/period/list'],
            ['Период #' . $period, '/admin/period/view/' . $period],
            ['Секции', '/admin/section/list?period_id=' . $period],
            ['Секция #' . $section, '/admin/section/view/' . $section],
            ['Номиналы', '/admin/nominal/list?section_id=' . $section],
            ['Номинал #' . $nominal, '/admin/nominal/view/' . $nominal],
            ['Разновидности', '/admin/coin/list?nominal_id=' . $nominal]
        ];

    }

    public function getViewLinks()
    {

        return [
        //    ['/admin/variety/list?coin_id=' . $this->id, 'Список Разновидностей'],
        //    ['/admin/variety/create?coin_id=' . $this->id, 'Добавить Разновидность']
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::with('nominal.section.period')->get() as $item){
                self::$_list[$item['id']] = $item->nominal->section->period->title . ' - ' . $item->nominal->section->title . ' - ' . $item->nominal->title . ' - ' . $item->title;
            }
        }

        return self::$_list;

    }

    public function varieties()
    {

        return $this->hasMany('\\App\\Models\\Variety');

    }

    public function setYearsAttribute($value)
    {

        $years = [];
        foreach(explode(',', $value) as $v){
            $v = explode('-', $v);
            if(1 === count($v)){
                $years[] = (int)preg_replace('/^[^0-9]*$/', '', $v[0]);
            } elseif(2 === count($v)){
                $start = (int)preg_replace('/^[^0-9]*$/', '', $v[0]);
                $end   = (int)preg_replace('/^[^0-9]*$/', '', $v[1]);
                if($start < $end){
                    while($start <= $end){
                        $years[] = $start;
                        $start++;
                    }
                }
            }
        }
        $this->attributes['years'] = implode(',', $years);

    }
}
