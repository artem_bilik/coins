<?php

namespace App\Models;

use App\Helpers\Html;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use App\Models\Manage\AttributesSetter;

class ViewsHistory extends Model
{
    protected $table = 'views_history';
    public $timestamps = false;

}
