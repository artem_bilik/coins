<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Section extends Model implements IManage
{

    use Manage;
    use AttributesSetter;


    protected $hidden = [
        'created_at', 'updated_at',
    ];

    private static $_list = [];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Секции',
            'view_title'  => function($country) {
                return "Секция - №" . $country->id;
            },
            'create_title' => 'Новая секция',
            'fields' => [
                [
                    'name'  => 'id',
                    'title' => 'ID',
                    'list'  => true,
                    'field' => false,
                    'search_by' => '=',
                ],
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'period_id',
                    'title' => 'Период',
                    'list' => true,
                    'field' => 'choice',
                    'choice_list' => Period::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Period::getList())),
                    'value' => function($period_id){
                        $periods = Period::getList();
                        if(array_key_exists($period_id, $periods)){
                            return $periods[$period_id];
                        } return 'Период не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'is_active',
                    'title' => 'Активен?',
                    'field' => 'checkbox',
                    'list'  => true,
                    'value' => function($value) {
                        return (1 == $value) ? 'Да' : 'Нет';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'field' => 'number',
                    'list'  => true,
                    'validators' => 'integer',
                    'search_by'  => '=',
                ]
            ],
        ];

    }

    public function getViewLinks()
    {

        return [
            ['/admin/nominal/list?section_id=' . $this->id, 'Список Номиналов'],
            ['/admin/nominal/create?section_id=' . $this->id, 'Добавить Номинал']
        ];

    }

    public function period()
    {

        return $this->belongsTo('\\App\Models\\Period');

    }

    public function nominals()
    {

        return $this->hasMany('\\App\\Models\\Nominal');

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::with('period')->get() as $item){
                self::$_list[$item->id] = $item->period->title . ' - ' . $item->title;
            }
        }

        return self::$_list;

    }

    public function getBreadcrumbs()
    {

        $period = array_get($_GET, 'period_id');
        if(!$period){
            $period = ($this->period_id) ? $this->period_id : 0;
        }
        return [
            ['Периоды', '/admin/period/list'],
            ['Период #' . $period, '/admin/period/view/' . $period],
            ['Секции', '/admin/section/list?period_id='.$period],
        ];

    }

    public function getNominalWithVarietiesInYear($year)
    {

        $nominals = Nominal::select('nominals.id')->where('section_id', '=', $this->id)->join('varieties', 'varieties.nominal_id', '=', 'nominals.id')->where('varieties.year', '=', $year)->get();
        $nominal_ids = [];
        foreach($nominals as $n){
            $nominal_ids[] = $n->id;
        }
        return Nominal::wherein('id', $nominal_ids)->active()->get();

    }
}
