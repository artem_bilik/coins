<?php

namespace App\Models;

use App\Models\Manage\AttributesSetter;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class Country extends Model implements IManage
{

    use Manage;
    use AttributesSetter;

    private static $_list = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Страны',
            'view_title'  => function($country) {
                return "Страна - №" . $country->id;
            },
            'create_title' => 'Новая страна',
            'fields' => [
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'sort',
                    'title' => 'Номер',
                    'list'  => true,
                    'field' => 'number',
                ],
            ],
        ];

    }

    public static function getList()
    {

        if(!self::$_list){
            foreach(self::all()->toArray() as $item){
                self::$_list[$item['id']] = $item['title'];
            }
        }

        return self::$_list;

    }

    public function auctions()
    {

        return $this->hasMany('\\App\\Models\\Auction');

    }
    public function getBreadcrumbs()
    {

        return [
            ['Страны', '/admin/country/list']
        ];

    }

}
