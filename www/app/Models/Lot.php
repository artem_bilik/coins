<?php

namespace App\Models;

use App\Helpers\Html;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;
use App\Models\Manage\ModelWithFile;
use DB;

class Lot extends Model implements IManage
{

    use Manage;
    use ModelWithFile;

    protected $attributes = array(
        'number' => 0,
    );

    public function getManageMap()
    {

        return [
            'list_title'  => 'Лоты',
            'view_title'  => function($item) {
                return "Лот - №" . $item->id;
            },
            'create_title' => 'Новый Лот',
            'fields' => [
                // [
                //     'name'  => 'number',
                //     'title' => 'Номер',
                //     'list'  => true,
                //     'field' => 'number',
                //     'validators' => 'required',
                // ],
                [
                    'name'  => 'title',
                    'title' => 'Название',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'variety_id',
                    'title' => 'Разновидность',
                    'list' => true,
                    'field' => 'text',
                    'value' => function($variety_id){
                        if($variety_id){
                            return Html::a('/admin/variety/view/'.$variety_id, $variety_id);
                        }
                        return '-';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'bargain_id',
                    'title' => 'Торг',
                    'list'  => true,
                    'field' => 'choice',
                    'choice_list' => Bargain::getList(),
                    'validators'  => 'required|in:' . implode(',', array_keys(Bargain::getList())),
                    'value' => function($bargain_id){
                        $bargains = Bargain::getList();
                        if(array_key_exists($bargain_id, $bargains)){
                            return $bargains[$bargain_id];
                        } return 'Торг не установлен.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'start_price',
                    'title' => 'Стартовая цена',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'price',
                    'title' => 'Окончательная цена',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name' => 'currency_id',
                    'title' => 'Валюта',
                    'list' => true,
                    'field' => 'choice',
                    'choice_list' => Currency::getList(),
                    'validators' => 'required|in:' . implode(',', array_keys(Currency::getList())),
                    'value' => function($currency_id){
                        $currencies = Currency::getList();
                        if(array_key_exists($currency_id, $currencies)){
                            return $currencies[$currency_id];
                        } return 'Валюта не установлена.';
                    },
                    'search_by' => '=',
                ],
                [
                    'name'  => 'description',
                    'title' => 'Описание',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                // [
                //     'name' => 'safety_id',
                //     'title' => 'Сохранность',
                //     'list' => true,
                //     'field' => 'choice',
                //     'choice_list' => Safety::getList(),
                //     'validators' => 'in:' . implode(',', array_keys(Safety::getList())),
                //     'value' => function($safety_id){
                //         $safeties = Safety::getList();
                //         if(array_key_exists($safety_id, $safeties)){
                //             return $safeties[$safety_id];
                //         } return 'Сохранность не установлена.';
                //     },
                //     'search_by' => '=',
                // ],
                [
                    'name'  => 'safety_text',
                    'title' => 'Сохранность (текст)',
                    'list'  => false,
                    'field' => 'text',
                ],
                [
                    'name'  => 'front_img',
                    'title' => 'Вид спереди',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image((new \App\Helpers\ImageCache($img, 100, 100))->getWebPath(), $this->title, []);
                    },
                ],
                [
                    'name'  => 'back_img',
                    'title' => 'Вид сзади',
                    'list'  => true,
                    'field' => 'file',
                    'validators' => 'image',
                    'filter' => '',
                    'messages' => [
                        'img.image' => 'Загрузите картинку'
                    ],
                    'value' => function($img){
                        return Html::image((new \App\Helpers\ImageCache($img, 100, 100))->getWebPath(), $this->title, []);
                    },
                ],

            ],
        ];

    }

    public function scopeSort($query)
    {

        return $query->orderBy('number', 'ASC');

    }

    public function bargain()
    {

        return $this->belongsTo('\\App\\Models\\Bargain');

    }

    public function safety()
    {

        return $this->belongsTo('\\App\\Models\\Safety');

    }

    public function currency()
    {

        return $this->belongsTo('\\App\\Models\\Currency');

    }

    public function getFileDir()
    {

        $bargain = \App\Models\Bargain::select(['id', 'auction_id'])->find($this->bargain_id);
        return 'auctions/' . $bargain->auction_id . '/bargains/' . $bargain->id . '/lots';

    }

    public function getBreadcrumbs()
    {

        $bargain = array_get($_GET, 'bargain_id');
        if(!$bargain){
            $bargain = ($this->bargain_id) ? $this->bargain_id : 0;
            $auction = ($this->bargain)    ? $this->bargain->auction_id : 0;
        } else {
            $bargain_m = Bargain::find($bargain);
            if($bargain_m){
                $auction = $bargain_m->auction_id;
            } else {
                $bargain = $auction = 0;
            }
        }

        return [
            ['Аукционы', '/admin/auction/list'],
            ['Аукцион #' . $auction, '/admin/auction/view/' . $auction],
            ['Торги', '/admin/bargain/list?auction_id=' . $auction],
            ['Торг #'.$bargain, '/admin/bargain/view/'.$bargain],
            ['Лоты', '/admin/lot/list?bargain_id='.$bargain],
        ];

    }
    public function save(array $options = [])
    {
        
        if(parent::save($options)) {
            DB::statement("update varieties v set lots_cnt=(select count(*) from lots where variety_id={$this->variety_id}) WHERE v.id={$this->variety_id}");
            DB::statement("update bargains b set lots_cnt=(select count(*) from lots where bargain_id={$this->bargain_id}) WHERE b.id={$this->bargain_id}");
            return true;
        } else {
            return false;
        }

    }

    public static function getLastLots()
    {

        return self::with(['safety', 'bargain'])->orderBy('id', 'desc')->limit(6)->get();

    }

    public function setInfoAttribute($value)
    {

        $this->attributes['info'] = json_encode($value);

    }

    public function getInfoAttribute($value)
    {

        return json_decode($value, true);

    }

    public function link()
    {

        try{
            // находим год
            $matches = null;
            preg_match_all('/[0-9]{4}/', $this->title, $matches);
            if(!empty($matches)){
                if(isset($matches[0]) && is_array($matches[0]) && 1 === count($matches[0])){
                    $year = $matches[0][0];
                }
            }
            if(!isset($year)){
                throw new \Exception('Год не определен.');
            }
            // находим период
            $info = $this->info;
            if(array_key_exists('period', $info)){
                $period = Period::getPeriodIdByGNumber($info['period']);
            }
            if(!$period){
                throw new \Exception('Период не определен.');
            }
            // находим номинал
            foreach(Nominal::getListByPeriod($period) as $n_id => $n_aliases){
                foreach($n_aliases as $n_alias){
                    if(false !== strpos(strtolower($this->title), $n_alias)){
                        $nominal_id = $n_id;
                        $nominal_title = $n_aliases[0];
                        break 2;
                    }
                }
            }
            if(!isset($nominal_id)){
                throw new \Exception('Номинал не определен.');
            }
            // находим монету
            $coins = Coin::where('nominal_id', $nominal_id)->where('years', 'like', '%'.$year.'%')->select('id')->get()->toArray();
            if(1 === count($coins)){
                $coin_id = $coins[0]['id'];
            }
            if(!isset($coin_id)){
                // Создаем монету
                $period = Period::find($period);
                if(!$period){
                    throw new \Exception('При создании монеты не найден период.');
                }
                $coin = new Coin;
                $coin->nominal_id = $nominal_id;
                $coin->title = $nominal_title .' ' . $period->start_year . '-' . $period->end_year;
                $coin->is_active = 1;
                $coin->sort = 0;
                $coin->years = $period->start_year . '-' . $period->end_year;
                $coin->save();
                $coin_id = $coin->id;
            }
            if(!isset($coin_id)){
                throw new \Exception('Монета не определена.');
            }
            // добавить в разновидность
            $bitkin = -1;
            // находим разновидность
            $variety = Variety::where('coin_id', $coin_id)->where('bitkin', $bitkin)->where('year', $year)->first();
            if(!$variety){
                $variety = new Variety();
                $variety->coin_id = $coin_id;
                $variety->year = $year;
                $variety->bitkin = $bitkin;
                $variety->is_active = 1;
                $variety->save();
            }
            $variety_id = $variety->id;
            $this->variety_id = $variety_id;
            $this->save();
            return $variety->id;
        } catch(\Exception $e){
            $this->error = $e->getMessage();
            $this->save();
            throw $e;
        }


    }

}
