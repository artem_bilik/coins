<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use App\Models\Manage\IManage;
use App\Models\Manage\Manage;

class User extends Authenticatable implements IManage
{

    use Manage;

    const ROLE_USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public $pass;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getManageMap()
    {

        return [
            'list_title'  => 'Пользователи',
            'view_title'  => function($user) {
                return "Пользователь - №" . $user->id;
            },
            'create_title' => 'Новый пользователь',
            'fields' => [
                [
                    'name'  => 'name',
                    'title' => 'Имя',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required',
                ],
                [
                    'name'  => 'email',
                    'title' => 'E-mail',
                    'list'  => true,
                    'field' => 'text',
                    'validators' => 'required|email' . ($this->id ? '' : '|unique:users'),
                    'messages' => [
                        'email.email'  => 'Введите валидный E-mail адрес.',
                        'email.unique' => 'Пользователь с таким E-mail адресом уже существует.'
                    ]
                ],
                [
                    'name'  => 'created_at',
                    'title' => 'Дата Регистрации',
                ],
                [
                    'name'  => 'role',
                    'title' => 'Роль',
                    'field' => 'choice',
                    'choice_list' => [
                        'user' => 'Пользователь',
                        'moderator' => 'Модератор',
                        'admin' => 'Администратор',
                    ],
                    'validators' => 'required|in:user,moderator,admin',
                ],
                [
                    'name'  => 'pass',
                    'title' => 'Пароль',
                    'value' => '-',
                    'field' => 'text',
                    'validators' => 'required'
                ],
            ],
        ];

    }

    public function setUpdateAttribute($name, $value)
    {

        if('pass' === $name){
            $this->password = Hash::make($value);
        } else {
            $this->$name = $value;
        }

    }

    public function save(array $options = [])
    {


        if(!$this->id){
            $this->role       = self::ROLE_USER;
            $this->created_at = date('Y-m-d H:i:s');
            $this->password   = Hash::make($this->pass);
        }

        if(!parent::save($options)){
            return false;
        } else {
            return true;
        }

    }
    public function getBreadcrumbs()
    {

        return [
            ['Пользователи', '/admin/user/list']
        ];

    }
}
