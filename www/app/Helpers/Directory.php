<?php
namespace App\Helpers;

class Directory
{

    private $_path = '';

    private function __construct($path)
    {

        $this->_path = $path;

    }

    public function getPath()
    {

        return $this->_path;

    }

    public static function get($path, $mode = 0775)
    {

        if(!is_dir($path)){
            if(!mkdir($path, $mode, true)){
                throw new \Exception('Cann\'t create directory: ' . $path . '.');
            }
        }

        return new self($path);

    }

}