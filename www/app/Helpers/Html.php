<?php
namespace App\Helpers;

class Html extends \artem_c\html\Html
{

    public static function formHorizontalText($name, $value, $label, $placeholder, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }
        $input = self::div(['class' => 'col-sm-10'], self::input('text', $name, $value, ['id' => $name . '_id', 'class' => 'form-control', 'placeholder' => $placeholder]) . $error_txt);
        $label = self::label(['class' => 'col-sm-2 control-label', 'for' => $name . '_id'], $label);
        $class = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }
        return self::div(['class' => $class], $label . $input);

    }

    public static function formHorizontalNumber($name, $value, $label, $placeholder, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }
        $input = self::div(['class' => 'col-sm-10'], self::input('number', $name, $value, ['id' => $name . '_id', 'class' => 'form-control', 'placeholder' => $placeholder]) . $error_txt);
        $label = self::label(['class' => 'col-sm-2 control-label', 'for' => $name . '_id'], $label);
        $class = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }
        return self::div(['class' => $class], $label . $input);

    }

    public static function formHorizontalDate($name, $value, $label, $placeholder, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }
        $input = self::div(['class' => 'col-sm-10'], self::input('date', $name, $value, ['id' => $name . '_id', 'class' => 'form-control', 'placeholder' => $placeholder]) . $error_txt);
        $label = self::label(['class' => 'col-sm-2 control-label', 'for' => $name . '_id'], $label);
        $class = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }
        return self::div(['class' => $class], $label . $input);

    }

    public static function formHorizontalSubmit($value)
    {

        return self::div(['class' => 'form-group'],
            self::div(['class' => 'col-sm-offset-2 col-sm-10'], self::button(['type' => 'submit', 'class' => 'btn btn-default'], $value))
        );

    }

    public static function formHorizontalSelect($name, $value, $list, $label, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }
        $select = self::div(['class' => 'col-sm-10'], self::select($name, $value, $list, ['id' => $name . '_id', 'class' => 'form-control']) . $error_txt);
        $label  = self::label(['class' => 'col-sm-2 control-label', 'for' => $name . '_id'], $label);
        $class  = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }
        return self::div(['class' => $class], $label . $select);

    }

    public static function formHorizontalCheckbox($name, $value, $label, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }
        $checkbox = self::label([], self::checkbox($name, $value) . $label) . $error_txt;

        $class  = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }

        return self::div(['class' => $class],
            self::div(['class' => 'col-sm-offset-2 col-sm-10'],
                self::div(['class' => 'checkbox'], $checkbox)
            )
        );

    }

    public static function formHorizontalFile($name, $label, array $errors)
    {

        $error_txt = '';
        foreach($errors as $error){
            $error_txt .= self::span(['class' => 'help-block'], $error);
        }

        $file = self::label(['class' => 'file'],
                self::input('file', $name, false, ['id' => 'file']) . self::span(['class' => 'file-custom'])
        );
        $file  = self::div(['class' => 'col-sm-10'], $file . $error_txt);
        $label = self::label(['class' => 'col-sm-2 control-label', 'for' => $name . '_id'], $label);
        $class  = 'form-group';
        if($error_txt){
            $class .= ' has-error';
        }
        return self::div(['class' => $class], $label . $file);


    }

}