<?php
namespace App\Helpers;

class Date
{

    public static function getMonthByText($month)
    {

        $months = ['01' => 'ян', '02' => 'фе', '03' => 'мар', '04' => 'ап', '05' => 'ма', '06' => 'июн', '07' => 'июл', '08' => 'ав', '09' => 'сен', '10' => 'ок', '11' => 'но', '12' => 'де'];
        foreach($months as $k => $m_t){
            if(false !== strpos($month, $m_t)){
                return $k;
            }
        }
        throw new \Exception('Не удалось определить номер месяца "' . $month . '"');

    }

}