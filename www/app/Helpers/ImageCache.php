<?php
namespace App\Helpers;

class ImageCache
{

    private $_path = '';

    public function __construct($path, $width, $height)
    {

        $full_path = realpath(base_path($path));
        if(!file_exists($full_path)){
            return null;
        }
        $dir = Directory::get(public_path(pathinfo($path, \PATHINFO_DIRNAME) . '/'))->getPath();
        $new_path = $dir . pathinfo($path, \PATHINFO_FILENAME) . '_' . $width . '_' . $height . '.' . pathinfo($path, \PATHINFO_EXTENSION);
        if(!file_exists($new_path)){
            $image = new \Imagick($full_path);
            $image->resizeImage($width, $height, \Imagick::FILTER_SINC, 1);
            $image->writeImage($new_path);
        }
        $new_path = realpath($new_path);
        $this->_path = str_replace(public_path(), '', $new_path);

    }

    public function getWebPath()
    {
        return $this->_path;
    }

    public function getPath()
    {

        return base_path($this->_path);

    }

}