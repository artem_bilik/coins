<?php

namespace App\Forms;

use artem_c\laraform\Field;
use App\Helpers\Html;
use Auth;
use App\Models\User;
class Login extends \artem_c\laraform\Form
{

    protected $_token = true;

    public function getFields()
    {

        return [
            new Field(
                'email',
                'required|email',
                'E-mail',
                function($name, $value, $label, $errors){
                    return Html::formHorizontalText($name, $value, $label, $label, $errors);
                }
            ),
            new Field(
                'password',
                'required',
                'Пароль',
                function($name, $value, $label, $errors){
                    return Html::formHorizontalText($name, $value, $label, $label, $errors);
                }
            ),
        ];

    }

    public function getHtml()
    {

        return Html::form(
            ['class' => 'form-horizontal', 'method' => 'post'],
            $this->getFieldsHtml() . Html::formHorizontalSubmit('Войти')
        );

    }

    public function messages()
    {

        return [
            'required'    => 'Это поле обязательно для заполнения',
            'email.email' => 'Введите валидный E-mail адрес',
        ];

    }

    public function login()
    {

        return Auth::attempt(['email' => $this->email, 'password' => $this->password]);

    }

}