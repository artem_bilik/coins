<?php
namespace App\Console\Commands\Grubber;

use Illuminate\Console\Command;

class Base extends Command
{

    public function __construct()
    {

        parent::__construct();
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            throw new \Exception("Ошибка #{$errno} - \"{$errstr}\" в файле {$errfile}:{$errline}");
        });

    }

    protected function logger($message, $level = 0)
    {

        echo str_repeat("\t", $level) . $message . "\t" . date('Y-m-d H:i:s') . "\n";

    }

}