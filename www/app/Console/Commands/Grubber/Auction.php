<?php
namespace App\Console\Commands\Grubber;

use \App\Models\Auction as Auct;

class Auction extends Base
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grub:auction {auction_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grub auction';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $auction = Auct::find($this->argument('auction_id'));
        if(null === $auction){
            throw new \Exception('Аукцион "' . $this->argument('auction_id') . '" не найден.');
        }
        $grubber_name = '\\App\\Grubber\\' . $auction->grubber_class_name;

        if(!class_exists($grubber_name)){
            throw new \Exception('Класс "' . $grubber_name . '" не найден.');
        }

        $grubber = new $grubber_name($auction);
        $grubber->grub();

    }

}