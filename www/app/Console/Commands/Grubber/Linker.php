<?php

namespace App\Console\Commands\Grubber;

use App\Models\Lot;

class Linker extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'link:lots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Link lots with varieties';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->logger('Начало.');
        $offset = 0;
        $limit = 100;
        do{
            $lots = Lot::where('variety_id', null)->skip($offset)->take($limit)->orderBy('id', 'ASC')->get();
            foreach($lots as $lot){
                $this->logger('Обрабатывается лот №' . $lot->id . '.', 1);
                try{
                    $this->logger('Привязано к разновидности - ' . $lot->link(), 2);
                    $lot->link();
                } catch(\Exception $e){
                    $this->logger('Ошибка - ' . $e->getMessage(), 2);
                }
                $last_id = $lot->id;
            }
            $offset += $limit;

        } while(!empty($lots));


        $this->logger('Конец.');

    }

}
