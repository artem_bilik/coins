<?php

namespace App\Console\Commands\Grubber;


use App\Helpers\Directory;
use App\Models\Auction;

class Grubber extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grub:auctions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grub auctions';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo "\n\n\n";
        $this->logger('Начало граббинга.');
        $auctions = Auction::all(['id', 'title', 'grubber_class_name'])->toArray();
        foreach($auctions as $auction) {
            try{
                $this->logger("Аукцион #{$auction['id']} - {$auction['title']}. Граббер - {$auction['grubber_class_name']}", 1);

                $log = Directory::get(storage_path('logs') . '/grubber/' . date('Y') . '/' . date('m') . '/' . date('d') . '/')->getPath() . $auction['grubber_class_name'] . '.log';

                $return_val = null;
                $output = [];

                exec('php ' . base_path() . '/artisan grub:auction ' . $auction['id'], $output, $return_val);

                file_put_contents($log, implode("\n", $output));
                if($return_val !== 0){
                    Auction::where('id', $auction['id'])->update(['grubber_error' => $return_val]);
                    throw new \Exception("Аукцион #{$auction['id']} - {$auction['title']}. Граббер - {$auction['grubber_class_name']} - Ошибка.");
                }

            } catch(\Exception $e){
                $this->logger($e->getMessage(), 1);
            }
            $this->logger("Аукцион #{$auction['id']} - Завершено.", 1);
        }
        // @todo запускаем линкер
        $this->logger('Завершено.');

    }

}
