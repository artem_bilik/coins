<?php

namespace App\Console\Commands;

use App\Models\Nominal;
use App\Models\Variety;
use Illuminate\Console\Command;
use DB;

class UploadVarieties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:varieties {file}';

    private $_nominals;
    private $number = 1;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload varieties from excel file.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $file = $this->argument('file');
        if(!is_file($file)){
            throw new \Exception('File "' . $file . '" Not Found!');
        }

        $Reader = \PHPExcel_IOFactory::createReaderForFile($file);
        $Reader->setReadDataOnly(true);
        $objXLS = $Reader->load($file);
        $sheet = $objXLS->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        DB::beginTransaction();
        try{
            for ($row = 1; $row <= $highestRow; $row++) {
                if($row === 1 || $row === 2) continue;
                $this->number = $row;
                $rowData = $sheet->rangeToArray('A' . $row . ':' . 'L' . $row, NULL, TRUE, FALSE);
                $this->saveVariety($rowData);
            }
            DB::commit();
        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }
        echo "Ok\n";


    }

    private function saveVariety($row)
    {

        $map = [
            'section_id', 'nominal', 'year', 'desc', 'metal', 'sample', 'weight', 'diameter', 'herd', 'bitkin_rarity', 'bitkin', 'uzdennikov'
        ];
        $data = ['section_id' => null, 'nominal' => null, 'year' => null, 'desc' => null, 'metal' => null, 'sample' => null, 'weight' => null, 'diameter' => null, 'herd' => null, 
            'bitkin_rarity' => null, 'bitkin' => null, 'uzdennikov' => null];
        foreach($row[0] as $k=>$v){
            if(array_key_exists($k, $map)){
                $data[$map[$k]] = $v;
            } else {
                throw new \Exception('Undefined key in excel map.');
            }
        }
        if(!$data['section_id'] && (!$data['nominal'] || $data['nominal'] == 'вторая книга')){
            return;
        }
        if(!$data['section_id']){
            $data['section_id'] = '96';
        }
        if($data['year'] == 'без года'){
            $data['year'] = 'none';
        }
        if($data['weight'] == '?'){
            $data['weight'] = null;
        }
        if($data['herd'] == '?'){
            $data['herd'] = null;
        }
        $variety = new Variety();
        $variety->nominal_id    = $this->getNominal($data['section_id'], $data['nominal']);
        $variety->year          = $data['year'];
        $variety->description   = $data['desc'];
        $variety->metal         = $data['metal'];
        $variety->sample        = $data['sample'];
        $variety->weight        = $data['weight'];
        $variety->diameter      = $data['diameter'];
        $variety->herd          = $data['herd']; 
        $variety->bitkin_rarity = $data['bitkin_rarity'];
        $variety->bitkin        = $data['bitkin'];
        $variety->uzdennikov    = $data['uzdennikov'];
        if(!$variety->save()){
            throw new \Exception('Variety was not saved.');
        }
        echo "Variety " . $this->number. " was saved. ID = " . $variety->id . "\n";

    }

    private function getNominal($section_id, $title)
    {

        if(null === $this->_nominals){
            $this->_nominals = Nominal::select(['section_id', 'id', 'title'])->get()->toArray();
        }
        foreach($this->_nominals as $nominal){
            if($nominal['section_id'] === $section_id && $nominal['title'] === $title){
                return $nominal['id'];
            }
        }
        $nominal_m = new Nominal();
        $nominal_m->title = $title;
        $nominal_m->section_id = $section_id;
        $nominal_m->is_active = 1;
        if($nominal_m->save()){
            $this->_nominals[] = ['section_id' => $section_id, 'title' => $title, 'id' => $nominal_m->id];
            return $nominal_m->id;
        }
        throw new \Exception('Nominal was not added.');

    }

}
