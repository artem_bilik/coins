<?php
namespace App\Grubber\Helpers;

use \App\Helpers\Directory;
class Lot
{

    private $_lot = [
        'number'      => '',
        'title'       => '',
        'start_price' => '',
        'end_price'   => '',
        'description' => '',
        'images'      => [],
        'currency'    => '',
    ];

    private $_info = [];

    public function __construct($number, $title, $start_price, $end_price, $description, array $images, $currency)
    {

        $this->_lot['number']      = $number;
        $this->_lot['title']       = strip_tags($title);
        $this->_lot['start_price'] = $start_price;
        $this->_lot['end_price']   = $end_price;
        $this->_lot['description'] = strip_tags($description);
        $this->_lot['images']      = $images;
        $this->_lot['currency']    = $currency;

    }
    public function save(\App\Models\Bargain $bargain)
    {

        $lot = new \App\Models\Lot();
        $lot->bargain_id  = (int)$bargain->id;
        $lot->number      = $this->_lot['number'];
        $lot->title       = $this->_lot['title'];
        $lot->start_price = (int)$this->_lot['start_price'];
        $lot->price       = (int)$this->_lot['end_price'];
        $lot->description = $this->_lot['description'];
        $lot->currency_id = $this->_lot['currency'];
        $lot->safety_id   = 1;
        $lot->title       = $this->_lot['title'];
        $lot->front_img   = 'error';
        $lot->back_img    = 'error';
        $lot->info        = $this->_info;
        $lot->save();
        if(!empty($this->_lot['images'])){
            foreach($this->_lot['images'] as $key => $image){
                $path = $this->getImgPath($bargain->auction_id, $bargain->id, (($key === 0 ) ? 'front_img' : 'back_img') . '_' . $lot->id . '.' . pathinfo($image, \PATHINFO_EXTENSION));
                $img = file_get_contents($image);
                if($img){
                    if(file_put_contents($path['full_path'], $img)) {
                        if($key === 0){
                            $lot->front_img = $path['path'];
                        }
                        if($key === 1){
                            $lot->back_img = $path['path'];
                        }
                    }
                }
                if($key === 1){
                    break;
                }
            }
            $lot->save();
        }

    }
    private function getImgPath($auction_id, $bargain_id, $file)
    {

        $sub_path = '/images/auctions/' . $auction_id . '/bargains/' . $bargain_id . '/lots/';
        $path = Directory::get(base_path() . $sub_path);
        $path_to_img = $sub_path . $file;
        $full_path = $path->getPath() . $file;

        return [
            'path' => $path_to_img,
            'full_path' => $full_path,
        ];

    }

    public function addPeriodToInfo($period_id)
    {

        $this->_info['period'] = (int)$period_id;

    }

}