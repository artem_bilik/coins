<?php
namespace App\Grubber\Helpers;

use DB;
use App\Helpers\Directory;

class Bargain
{

    private $_auction_id = 0;
    private $_bargain = [
        'info' => '',
        'title' => '',
        'number' => '',
        'date' => '',
        'place' => '',
        'img' => '',
        'is_closed' => false,
    ];
    private $_lots = [];

    public function __construct($auction_id)
    {

        $this->_auction_id = (int)$auction_id;

    }
    public function setInfo($info)
    {

        $this->_bargain['info'] = (string)$info;

    }

    public function setImg($src)
    {

        $this->_bargain['img'] = $src;

    }

    public function getInfo()
    {

        return $this->_bargain['info'];

    }

    public function setIsClosed($is_closed)
    {

        $this->_bargain['is_closed'] = (boolean)$is_closed;

    }

    public function isClosed()
    {

        return $this->_bargain['is_closed'];

    }

    public function setTitle($title)
    {

        $this->_bargain['title'] = $title;

    }
    public function setNumber($number)
    {

        $this->_bargain['number'] = $number;

    }
    public function setPlace($place)
    {

        $this->_bargain['place'] = $place;

    }
    public function setDate($date)
    {

        $this->_bargain['date'] = $date;

    }

    public function addLot(Lot $lot)
    {

        $this->_lots[] = $lot;

    }

    public function save()
    {

        DB::beginTransaction();
        try{

            $bargain = $this->saveBargain();
            foreach($this->_lots as $lot){
                $lot->save($bargain);
            }
            DB::commit();
        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

    }

    private function saveBargain()
    {

        $bargain = new \App\Models\Bargain();
        $bargain->auction_id = $this->_auction_id;
        $bargain->title      = $this->_bargain['title'];
        $bargain->number     = $this->_bargain['number'];
        $bargain->img        = 'error';
        $bargain->place_country = '';
        $bargain->place_city = $this->_bargain['place'];
        $bargain->date_open  = $this->_bargain['date'];
        $bargain->date_close = $this->_bargain['date'];
        $bargain->lots_cnt   = count($this->_lots);
        $bargain->save();

        if($this->_bargain['img']){
            $path = Directory::get(base_path() . '/images/auctions/' . $this->_auction_id . '/bargains/' . $bargain->id);
            $path_to_img = '/images/auctions/' . $this->_auction_id . '/bargains/' . $bargain->id . '/img_' . $bargain->id . '.' . pathinfo($this->_bargain['img'], \PATHINFO_EXTENSION);
            $full_path = $path->getPath() . '/img_' . $bargain->id . '.' . pathinfo($this->_bargain['img'], \PATHINFO_EXTENSION);
            $img = file_get_contents($this->_bargain['img']);
            if($img){
                if(file_put_contents($full_path, $img)) {
                    $bargain->img = $path_to_img;
                    $bargain->save();
                }
            }

        }
        return $bargain;

    }

}