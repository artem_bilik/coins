<?php
namespace App\Grubber\Helpers;

use PHPHtmlParser\Dom;
class Html
{

    private $_parser = null;
    public function __construct()
    {

        $this->_parser = new Dom;

    }

    public function loadUrl($url)
    {

        $this->_parser->loadFromUrl($url);

    }

    public function findOne($selector)
    {

        foreach($this->_parser->find($selector) as $data){
            return $data;
        }
        return null;

    }

    public function find($selector)
    {

        foreach($this->_parser->find($selector) as $item){
            yield $item;
        }

    }

    public function findIn($selector, $obj)
    {

        foreach($obj->find($selector) as $item){
            yield $item;
        }

    }

    public function findOneIn($selector, $object)
    {

        foreach($object->find($selector) as $data){
            return $data;
        }
        return null;

    }

    public function getHtmlFrom($object)
    {

        return $object->innerHtml;

    }

    public function getAttrFrom($attr, $obj)
    {
        return $obj->getAttribute($attr);

    }

    public function tagsToSpace($html)
    {

        return preg_replace("(<[^<>]+>)", ' ', $html);

    }

}