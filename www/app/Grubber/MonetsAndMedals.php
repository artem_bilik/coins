<?php
namespace App\Grubber;

use App\Helpers\Date;
use App\Grubber\Helpers\Html;
use App\Grubber\Helpers\Bargain;
use App\Grubber\Helpers\Lot;

class MonetsAndMedals extends Base
{
    private static $_cur_list = [
        '$' => 'usd'
    ];
    public function grub()
    {

        $this->logger('Грабим аукцион ' . $this->_auction->id . '.');
        $last_bargain = $this->getLastGrubbedBargain();
        $next_bargain = (!$last_bargain) ? 1 : ((int)$last_bargain->number + 1);
        while(true){
            $this->logger('Грабим торг №' . $next_bargain, 1);
            $bargain = $this->grubBargain($next_bargain);
            if(!$bargain->isClosed()){
                $this->logger('Торг №' . $next_bargain . ' еще не окончен.', 1);
                break;
            }
            $this->logger('Грабим лоты торга №' . $next_bargain, 2);
            $this->grubLots($bargain, $next_bargain);
            $this->logger('Сохраняем торг №' . $next_bargain, 1);
            $bargain->save();
            $next_bargain++;
        }

    }

    private function grubBargain($number)
    {

        $bargain = new Bargain($this->_auction->id);
        $html = new Html;
        $html->loadUrl('http://www.numismat.ru/au.shtml?au=' . (int)$number);
        $bargain_html = $html->findOne('#au-info');
        if(!$bargain_html){
            throw new \Exception('Не найдена информация о торге №' . $number . '.');
        }
        $info = $html->findOneIn('p', $bargain_html);
        if($info){
            $bargain->setInfo($html->tagsToSpace($html->getHtmlFrom($info)));
        }
        $this->setBargainInfo($bargain);
        $img = $html->findOneIn('img', $bargain_html);
        if($img){
            $bargain->setImg('http://www.numismat.ru' . $html->getAttrFrom('src', $img));
        }

        return $bargain;

    }

    private function grubLots(Helpers\Bargain $bargain, $number)
    {

        // парсим по периодам
        $periods = [130 => 1, 140 => 2, 150 => 3, 160 => 4, 170 => 5, 180 => 6, 190 => 7, 200 => 8, 210 => 9, 220 => 10, 230 => 11, 240 => 12, 250 => 13, 260 => 14];
        $types = [30, 35];
        foreach($periods as $period => $period_number){
            // parse
            $url = 'http://www.numismat.ru/au.shtml?au=' . $number . '&per=' . $period . '&descr=&material=0&nominal=0&ordername=0&orderdirection=ASC&num=44&lottype=';
            foreach($types as $type){
                $url .= $type;
                $page = 1;
                while(true){
                    $link = $url . '&page=' . $page;
                    $html = new Html;
                    $html->loadUrl($link);
                    // get lots
                    $lots = $html->findOne('#zapisi');
                    if(!$html->getHtmlFrom($lots)){
                        break;
                    }
                    foreach($html->findIn('.zapis', $lots) as $lot){
                        $this->setLotInfo($bargain, $lot, $html, $period_number);
                    }
                    $page++;
                }
                // add to bargain
            }
        }

    }

    private function setLotInfo(Helpers\Bargain $bargain, $data, $html, $period_number)
    {

        // номер
        $number = preg_replace('~\D+~','',$html->getHtmlFrom($html->findOneIn('h3', $data)));
        $description = $html->getHtmlFrom($html->findOneIn('.lot_txt p', $data));
        $desc_ar = explode('.', $description);
        $title = $desc_ar[0];
        $start_price = 0;
        $end_price = 0;

        foreach($html->findIn('.shop-priceN p .price', $data) as $key => $price){
            $pr = $html->getHtmlFrom($price);
            if(0 === $key){
                $pr = explode('–', $pr);
                $pr = $this->getPrice($pr[0]);
                $currency = $pr['currency'];
                $start_price = $pr['price'];
            }
            if(1 === $key){
                $end_price = (int)preg_replace('~\D+~','',$pr);
            }
        }
        if(!isset($currency)){
            foreach($html->findIn('.shop-priceN p', $data) as $key => $p){

                foreach(explode('<br />', $html->getHtmlFrom($p)) as $k => $price){
                    $pr = preg_replace('/(<b(.+)b>)/', '', $price);


                    if(0 === $k){
                        $pr = explode('–', $pr);

                        $pr = $this->getPrice($pr[0]);

                        $currency = $pr['currency'];
                        $start_price = $pr['price'];
                    }
                    if(1 === $k){
                        $end_price = (int)preg_replace('~\D+~','',$pr);
                    }
                }
            }
        }
        if(!isset($currency)){
            throw new \Exception('Валюта не определена.');
        }

        $images = [];
        foreach($html->findIn('.shop-pic img', $data) as $image){
            $images[] = 'http://www.numismat.ru' . $html->getAttrFrom('src', $image);
        }

        $lot = new Lot($number, $title, $start_price, $end_price, $description, $images, $currency);
        $lot->addPeriodToInfo($period_number);
        $bargain->addLot($lot);

    }

    private function setBargainInfo(Helpers\Bargain $bargain)
    {

        $info = $bargain->getInfo();
        $bargain->setIsClosed((boolean)strpos($info, 'состоялся'));
        $info = explode(' ', $info);
        $number = $date = $title = $place = '';

        $i = 0;
        foreach($info as $k => $v){
            unset($info[$k]);
            if('' === $v) { continue; }
            if($v == 'состоялся') {
                break;
            }
            $title .= $v . ' ';

            if('№' == mb_substr($v, 0, 1)){
                $number = (int)str_replace('№', '', $v);
            }
        }
        foreach($info as $k => $v){
            unset($info[$k]);
            if('' === $v) { continue; }
            if($v == 'г.') {
                break;
            }
            $date .= $v . ' ';
        }

        $date = explode(' ', trim($date));
        if(3 === count($date)){
            $date = (int)$date[2] . '-' . Date::getMonthByText($date[1]) . '-' . (int)$date[0];
        } else {
            throw new \Exception('Не удалось определить дату.');
        }

        foreach($info as $k => $v){
            unset($info[$k]);
            if('' === $v) { continue; }
            $place .= $v . ' ';
        }

        $bargain->setTitle(trim($title));
        $bargain->setPlace(trim($place));
        $bargain->setDate($date);
        $bargain->setNumber($number);

    }

    protected function getPrice($price_data, $number = 0)
    {

        $price_data = str_replace(' ', '', $price_data);
        $currency = '';

        if(preg_match('/^[0-9]+$/', $price_data)){
            $currency = 'ru';
        } else {
            foreach(self::$_cur_list as $cur_sign => $cur){
                if(false !== strpos($price_data, $cur_sign)){

                    $currency = $cur;
                }
            }
        }
        if(!array_key_exists($currency, $this->_currencies)){
            throw new \Exception('Валюта не определена. ' . $price_data);
        }

        return [
            'currency' => $this->_currencies[$currency],
            'price' => (int)preg_replace('~\D+~','',$price_data),
        ];


    }

}
