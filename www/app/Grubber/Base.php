<?php
namespace App\Grubber;

use App\Models\Auction;
use App\Models\Bargain;
use App\Models\Currency;

abstract class Base
{

    protected $_auction = null;
    protected $_currencies = [];


    public function __construct(Auction $auction)
    {

        $this->_auction = $auction;
        foreach(Currency::all() as $cur){
            $this->_currencies[$cur->sign] = $cur->id;
        }

    }

    protected function getLastGrubbedBargain()
    {

        return Bargain::where('auction_id', $this->_auction->id)->orderBy('number', 'DESC')->limit(1)->first();

    }

    protected function logger($message, $level = 0)
    {

        echo str_repeat("\t", $level) . $message . "\t" . date('Y-m-d H:i:s') . "\n";

    }




}