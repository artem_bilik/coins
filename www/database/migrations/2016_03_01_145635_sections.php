<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id');
            $table->string('title');
            $table->tinyInteger('is_active')->default(0);

            $table->timestamps();

            $table->foreign('period_id')->references('id')->on('periods');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('sections');

    }
}
