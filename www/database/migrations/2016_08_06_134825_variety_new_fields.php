<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VarietyNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('varieties', function (Blueprint $table) {
            $table->dropForeign('varieties_herd_id_foreign');
            $table->dropColumn('herd_id');
            $table->tinyInteger('herd')->nullable();
            $table->dropColumn('Bitkin_rating');
            $table->string('bitkin_rarity', 4)->nullable();
            $table->string('sample', 45)->nullable()->change();
            $table->string('bitkin', 45)->change();
            $table->string('weight', 45)->nullable()->change();
            $table->string('diameter', 45)->nullable()->change();
            $table->dropUnique('varieties_bitkin_unique');
            $table->string('metal', 255)->nullable()->change();
            $table->string('year', 5)->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('varieties', function (Blueprint $table) {
            $table->integer('herd_id');
            $table->foreign('herd_id')->references('id')->on('herds');
            $table->string('Bitkin_rating');
            $table->dropColumn('herd');
            $table->dropColumn('bitkin_rarity');
            $table->unique('bitkin');
        });

    }
}
