<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LotSafetyKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('lots', function (Blueprint $table) {
            $table->dropColumn('safety');
            $table->integer('safety_id');
            $table->foreign('safety_id')->references('id')->on('safeties');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('lots', function (Blueprint $table) {
            $table->dropForeign('lots_safety_id_foreign');
            $table->dropColumn('safety_id');
            $table->string('safety')->default('');
        });

    }
}
