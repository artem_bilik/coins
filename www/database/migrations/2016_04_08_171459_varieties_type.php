<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VarietiesType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table("varieties", function($table){
            $table->integer('bitkin')->nullable()->change();
            $table->integer('uzdennikov')->nullable();
            $table->unique('bitkin');
            $table->unique('uzdennikov');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table("varieties", function($table){
            $table->smallInteger('bitkin')->nullable()->change();
            $table->dropColumn('uzdennikov');
            $table->dropUnique('varieties_bitkin_unique');
        });

    }
}
