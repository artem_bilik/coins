<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Periods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->smallInteger('start_year');
            $table->smallInteger('end_year');
            $table->tinyInteger('is_active')->default(0);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('periods');

    }
}
