<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nominal_id');
            $table->string('title');
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('sort')->default(0);
            $table->timestamps();

            $table->foreign('nominal_id')->references('id')->on('nominals');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('coins');

    }
}
