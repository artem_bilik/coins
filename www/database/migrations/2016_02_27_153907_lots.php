<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('lots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bargain_id');
            $table->smallInteger('number');
            $table->double('start_price', 64, 2);
            $table->double('price', 64, 2);
            $table->mediumText('description');
            $table->string('front_img');
            $table->string('back_img');
            $table->string('safety');

            $table->timestamps();

            $table->foreign('bargain_id')->references('id')->on('bargains');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('lots');

    }
}
