<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VarietiyIdForLot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('lots', function($table){

            $table->integer('variety_id')->nullable();
            $table->foreign('variety_id')->references('id')->on('varieties');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('lots', function($table){
            $table->dropForeign('lots_variety_id_foreign');
            $table->dropColumn('variety_id');
        });

    }
}
