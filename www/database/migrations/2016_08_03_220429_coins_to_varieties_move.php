<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoinsToVarietiesMove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('varieties', function (Blueprint $table) {
            $table->dropForeign('varieties_coin_id_foreign');
            $table->dropColumn('coin_id');
            $table->integer('nominal_id');
            $table->foreign('nominal_id')->references('id')->on('nominals');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('varieties', function (Blueprint $table) {
            $table->dropForeign('varieties_nominal_id_foreign');
            $table->dropColumn('nominal_id');
            $table->integer('coin_id');
            $table->foreign('coin_id')->references('id')->on('coins');

        });

    }
}
