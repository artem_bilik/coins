<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultForVarietiesImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('varieties', function ($table) {
            $table->string('front_img')->default('')->change();
            $table->string('back_img')->default('')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('varieties', function ($table) {
            $table->string('front_img')->nullable()->change();
            $table->string('back_img')->nullable()->change();
        });

    }
}
