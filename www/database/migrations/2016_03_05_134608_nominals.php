<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nominals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('nominals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id');
            $table->string('title');
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('sort')->default(0);
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('nominals');

    }
}
