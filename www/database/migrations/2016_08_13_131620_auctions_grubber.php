<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AuctionsGrubber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('auctions', function(Blueprint $table) {
            $table->smallInteger('last_grubbed')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auctions', function(Blueprint $table) {
            $table->dropColumn('last_grubbed');
        });
    }
}
