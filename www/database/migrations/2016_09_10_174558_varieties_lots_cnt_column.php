<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VarietiesLotsCntColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('varieties', function (Blueprint $table) {
            $table->smallInteger('lots_cnt')->default(0);
        });
        DB::statement("update varieties v set lots_cnt=(select count(*) from lots where variety_id=v.id)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('varieties', function (Blueprint $table) {
            $table->dropColumn('lots_cnt');
        });

    }
}
