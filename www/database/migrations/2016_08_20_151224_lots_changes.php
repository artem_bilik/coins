<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LotsChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lots', function(Blueprint $table) {
            $table->string('start_price')->default('')->change();
            $table->string('price')->default('')->change();
            $table->string('safety_text')->default('');
            $table->integer('safety_id')->nullable()->change();
        });
        DB::table('currencies')->insert([
                'id'    => 1,
                'title' => 'Американский Доллар',
                'sign'  => '$',
                'abbr'  => 'USD'
            ]
        );
        DB::table('currencies')->insert([
                'id'    => 2,
                'title' => 'Российский Рубль',
                'sign'  => 'Р',
                'abbr'  => 'Р'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lots', function(Blueprint $table) {
           $table->dropColumn('safety_text');
        });
        DB::table('currencies')->where('id', '<', 3)->delete();
    }
}
