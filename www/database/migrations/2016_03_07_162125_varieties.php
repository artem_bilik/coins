<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Varieties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('varieties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coin_id');
            $table->string('year', 4)->default('--');
            $table->string('description', 1000);
            $table->string('metal')->default('-');
            $table->string('sample')->default('-');
            $table->string('weight')->default('-');
            $table->string('diameter')->default('-');
            $table->integer('herd_id');
            $table->string('Petrov_rating')->default('-');
            $table->string('Ilyin_rating')->default('-');
            $table->string('Bitkin_rating')->default('-');
            $table->string('front_img');
            $table->string('back_img');
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('sort')->default(0);

            $table->timestamps();

            $table->foreign('coin_id')->references('id')->on('coins');
            $table->foreign('herd_id')->references('id')->on('herds');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('varieties');

    }
}
