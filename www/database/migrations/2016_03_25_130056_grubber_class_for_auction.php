<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrubberClassForAuction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('auctions', function($table){
            $table->string('grubber_class_name')->default('none');
            $table->tinyInteger('grubber_error')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('auctions', function($table){
            $table->dropColumn('grubber_class_name');
            $table->dropColumn('grubber_error');
        });

    }
}
