<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bargains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('bargains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auction_id');
            $table->string('title');
            $table->smallInteger('number');
            $table->string('img');
            $table->string('place_country');
            $table->string('place_city');
            $table->date('date_open');
            $table->date('date_close');
            $table->smallInteger('lots_cnt');
            $table->timestamps();

            $table->foreign('auction_id')->references('id')->on('auctions');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('bargains');

    }
}
