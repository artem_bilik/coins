package directory
import (
	"os"
)
func Get(path string) error {
	err := os.MkdirAll(path, 0755)
	if nil != err {
		return err
	}
	return nil
}