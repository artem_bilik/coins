package logger

import "fmt"
import "time"

func print(message string, level uint8){
	var i uint8
    for i = 1;i <= level;i++{
        message = "\t" + message
    }
    message = message + "\t#" + time.Now().String() + "\n"
    fmt.Printf(message)
}

func Info(message string, level uint8){
    print(message, level)
}
func Error(err error, level uint8){
	print("Error: " + err.Error(), level)
    panic("Exit.")
}
func ErrorMessage(message string, level uint8){
	print("Error: " + message, level)
    panic("Exit.")
}
func Warning(message string, level uint8){
	message = "Warning: " + message
	print(message, level)
}
