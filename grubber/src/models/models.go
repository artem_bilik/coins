package models

import (
	"database/sql"
)

// Auction Model
type Auction struct {
    Id                  int     `gorm:"primary_key"`
    Title               string  `gorm:"size:255"` 
    Grubber_class_name  string  `gorm:"size:255"`
    Grubber_error       uint8
    Last_grubbed        int
}

// Bargain Model

type Bargain struct {
	Id            int `gorm:"primary_key"`
	Auction_id    int
	Title         string
	Number        int
	Img           string
	Place_country string
	Place_city    string
	Date_open     string
	Date_close    string
	Lots_cnt      int
}

// Lot Model
type Lot struct {
	Id          int `gorm:"primary_key"`
	Bargain_id  int
	Number      int
	Start_price string  `gorm:"size:255"` 
	Price       string  `gorm:"size:255"` 
	Description string
	Front_img   string  `gorm:"size:255"` 
	Back_img    string  `gorm:"size:255"` 
	Currency_id int
	Safety_id   sql.NullInt64
	Title       string  `gorm:"size:255"` 
	Variety_id  int
	Error       string  `gorm:"size:255"` 
	Safety_text string  `gorm:"size:255"` 
}

