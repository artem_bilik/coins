package grubber

import (
	"strconv"
	"strings"
	"errors"
	"regexp"
	"path/filepath"
	"net/http"
	"io/ioutil"
	 // "os"
	 // "fmt"

	"models"
	"logger"
	"directory"

	"github.com/PuerkitoBio/goquery"
	"github.com/jinzhu/gorm"
	iconv "github.com/djimenez/iconv-go"
)
const LOG_LEVEL uint8 = 3
type Grubber struct{
	Auction models.Auction
	DB *gorm.DB
	varieties [17][]map[string]string
}
type Lot struct {
	start_price string
	price       string
	description string
	front_img   string
	back_img    string
	title       string
	bitkin      string
	error       string
	currency    string
	period      int
	safety_text string
}


func (gr *Grubber) setVarieties() {
	var (
		id int
		period int
		bitkin string
	)
	sql := "SELECT v.id, p.id as period, v.bitkin FROM varieties v INNER JOIN nominals n ON n.id = v.nominal_id INNER JOIN sections s ON s.id = n.section_id INNER JOIN periods p ON p.id = s.period_id"
	rows, err := gr.DB.Raw(sql).Rows()
	if nil != err {
		logger.Error(err, LOG_LEVEL)
	}
	defer rows.Close()
	for rows.Next() {
    	rows.Scan(&id, &period, &bitkin)
    	element := map[string]string{
    		"id"     : strconv.Itoa(id),
    		"bitkin" : bitkin,
    	}
    	gr.varieties[period] = append(gr.varieties[period], element)
	}
}
func (gr *Grubber) getVariety(period int, bitkin string) int {
	if 17 > period {
		for _,element := range gr.varieties[period] {
			if bitkin == element["bitkin"] {
				id, _ := strconv.Atoi(element["id"])
				return id
			}
		}
	}
	return 0
}
func (gr *Grubber) Grub(){
	if 0 == gr.Auction.Grubber_error {
		gr.setVarieties()
		last_grubbed := gr.Auction.Last_grubbed + 1
		grub := true
		for true == grub {
			number := strconv.Itoa(last_grubbed)
			logger.Info("Grub bargain #" + number, LOG_LEVEL);

			gr.DB.Model(&gr.Auction).Update("grubber_error", 1)
			info, err := getBargain(gr.Auction.Grubber_class_name, number)
			if nil != err {
				logger.Warning(err.Error(), LOG_LEVEL)
				break
			}
			if "true" == info["is_done"]{
				logger.Info("Bargain is ended. Starting...", LOG_LEVEL);
				// start transaction
				tx := gr.DB.Begin()
				//createBargain(info)
				info["auction_id"] = strconv.Itoa(gr.Auction.Id)
				bargain_id, err := createBargain(info, tx)
				if nil != err {
					tx.Rollback()
					logger.Warning("Bargain was not created. " + err.Error(), LOG_LEVEL)
					break
				}
				err = gr.downloadAndSaveLots(gr.Auction.Grubber_class_name, bargain_id, number, tx)
				if nil != err {
					tx.Rollback()
					logger.Warning("Bargain's lots download error. " + err.Error(), LOG_LEVEL)
					break
				}
				// set auction grub status and last grubbed bargain
				// update lots cnt
				tx.Model(&gr.Auction).Update("grubber_error", 0)
				tx.Model(&gr.Auction).Update("last_grubbed", last_grubbed)
				tx.Commit()
				last_grubbed++
				grub = false
			} else {
				logger.Info("Bargain is not ended.", LOG_LEVEL);
				grub = false
				gr.DB.Model(&gr.Auction).Update("grubber_error", 0)
			}

		}
		logger.Info("Done", LOG_LEVEL)
	} else {
		logger.Warning("Previous grubbing error. Fix it before start", LOG_LEVEL)
	}
}
func getBargain(class_name string,number string) (map[string]string, error) {
	switch class_name{
		case "MonetsAndMedals": return getMonetsAndMedalsBargain(number)
		default: return nil, errors.New("Grubber Class handler not defined.")
	}
}
func createBargain(info map[string]string, db *gorm.DB) (int, error) {
	bargain := new(models.Bargain)

	auction_id, err := strconv.Atoi(info["auction_id"])
	if nil != err {
		return 0, err
	}
	number, err := strconv.Atoi(info["number"])
	if nil != err {
		return 0, err
	}
	bargain.Title         = info["title"]
	bargain.Auction_id    = auction_id
	bargain.Number        = number
	bargain.Place_country = info["country"]
	bargain.Place_city    = info["city"]
	bargain.Date_open     = info["date_start"]
	bargain.Date_close    = info["date_end"]
	bargain.Lots_cnt      = 0
	bargain.Img           = "error"

	if err = db.Create(&bargain).Error; nil != err {
		return 0, err
	}

	dir := "/images/auctions/" + strconv.Itoa(bargain.Auction_id) + "/bargains/" + strconv.Itoa(bargain.Id) + "/"
	//base_path := "/home/www/sites/coins/www"
	base_path := "/var/www/coins/www"
	err = directory.Get(base_path + dir)
	if nil != err {
		return 0, err
	}

	ext := filepath.Ext(info["img"])
	file_name := "img_" + strconv.Itoa(bargain.Id) + ext
	
	if err = saveImg(info["img"], base_path + dir + file_name); nil != err {
		return 0, err
	}
	if err = db.Model(&bargain).Update("img", dir + file_name).Error; nil != err {
		return 0, err
	}

	return bargain.Id, nil
}
func (gr *Grubber) downloadAndSaveLots(class_name string, bargain_id int, number string, db *gorm.DB) error {
	switch class_name{
		case "MonetsAndMedals": 
			lots, err := getMonetsAndMedalsLots(number)
			if nil != err {
				return err
			}

			for i := 0; i < len(lots); i++ {
				for k := 0; k < len(lots[i]); k++ {
					err = gr.saveLot(lots[i][k], bargain_id, db)
					if nil != err {
						return err
					}
				}
			}
		default: return errors.New("Class handler not defined.")
	}
	
	return nil
}
func (gr *Grubber) saveLot(lot Lot, bargain_id int, db *gorm.DB) error {
	var model = new(models.Lot)
	model.Bargain_id  = bargain_id
	model.Start_price = lot.start_price
	model.Price       = lot.price
	model.Description = lot.description
	model.Front_img   = "error"
	model.Back_img    = "error"



	if "usd" == lot.currency {
		model.Currency_id = 1
	} else if "rur" == lot.currency {
		model.Currency_id = 2
	} else {
		return errors.New("Currency not defined. - " + lot.description)
	}
	model.Title       = lot.title
	model.Variety_id  = gr.getVariety(lot.period, lot.bitkin)
	if 0 == model.Variety_id {
		logger.Info("Variety not found. - period - " + strconv.Itoa(lot.period) + " - "  + lot.description, LOG_LEVEL)
		return nil
		return errors.New("Variety not found. - period - " + strconv.Itoa(lot.period) + " - "  + lot.description)
	}
	model.Error       = lot.error
	model.Safety_text = lot.safety_text

	if 255 < len(model.Title)  {
		return errors.New("Title value too long - " + model.Title)
	}
	if 255 < len(model.Error)  {
		return errors.New("Error value too long - " + model.Error)
	}
	if 255 < len(model.Safety_text)  {
		return errors.New("Safety_text value too long - " + model.Safety_text)
	}


	if err := db.Create(&model).Error; nil != err {
		return err
	}

	dir := "/images/auctions/" + strconv.Itoa(gr.Auction.Id) + "/bargains/" + strconv.Itoa(bargain_id) + "/lots"
	//base_path := "/home/www/sites/coins/www"
	base_path := "/var/www/coins/www"
	err := directory.Get(base_path + dir)
	if nil != err {
		return err
	}
	// Save Front Img
	if "" != lot.front_img {
		f_ext := filepath.Ext(lot.front_img)
		f_name := "front_img_" + strconv.Itoa(model.Id) + f_ext
		if err := saveImg(lot.front_img, base_path + dir + f_name); nil != err {
			return err
		}
		if err := db.Model(&model).Update("front_img", dir + f_name).Error; nil != err {
			return err
		}
	}
	// Save Back Img
	if "" != lot.back_img {
		b_ext := filepath.Ext(lot.back_img)
		b_name := "back_img_" + strconv.Itoa(model.Id) + b_ext
		if err := saveImg(lot.back_img, base_path + dir + b_name); nil != err {
			return err
		}
		if err := db.Model(&model).Update("back_img", dir + b_name).Error; nil != err {
			return err
		}
	}
	return nil
}
func saveImg(url string, path string) error {
	resp, err := http.Get(url)
	if nil != err {
		for i := 1; i < 4; i++ {
			resp, err = http.Get(url)
  			if err == nil {
  				break
  			}
		}
		if nil != err {
			return err
		}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		return err
	}
	err = ioutil.WriteFile(path, body, 0644)
	return err
}

func getHtml(url string) (*goquery.Document, error){
	doc, err := goquery.NewDocument(url) 
	if nil != err {
		for i := 1; i < 4; i++ {
			doc, err := goquery.NewDocument(url) 
  			if err == nil {
  				return doc, nil
  			}
		}
		return nil, err
	} else {
		return doc, nil
	}
}

func getDate(date_str string) (string, error) {
	monthes := map[string]string{
		"01": "ян",
		"02": "фе",
		"03": "мар",
		"04": "ап",
		"05": "ма",
		"06": "июн",
		"07": "июл",
		"08": "ав",
		"09": "сен",
		"10": "ок",
		"11": "но",
		"12": "де",
	}
	date := strings.Split(date_str, " ")
	if 4 != len(date) {
		return "", errors.New("Wrong date length. '" + date_str + "'")
	}
    matched, err := regexp.MatchString("^[0-9]{1,2}$", date[0])
	if nil != err || false == matched {
		return "", errors.New("Wrong Day")
	}
	if 1 == len(date[0]) {
		date[0] = "0" + date[0]
	}
	matched, err = regexp.MatchString("^[0-9]{4,4}$", date[2])
	if nil != err || false == matched {
		return "", errors.New("Wrong year.")
	}
	for i,val := range monthes {
		if true == strings.Contains(date[1], val) {
			date[1] = i
			return date[2] + "-" + date[1] + "-" + date[0], nil
		}
	}
	return "", errors.New("Wrong month.")
}

////////////////////////////////////////////////////////////////////////////
// Bargains Functions
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
// MonetsAndMedals
func getMonetsAndMedalsBargain(number string) (map[string]string, error) {
	info := make(map[string]string)
	doc, err := getHtml("http://www.numismat.ru/au.shtml?au=" + number)
	if nil != err {
		return info, errors.New("Cann't get Bargain #" + number + ". " + err.Error())
	}
	
  	html, err :=  doc.Find("#au-info p").Html()
  	if nil != err || "" == html {
  		return info, errors.New("Bargain not found in HTML.")
  	}
	html = convert("windows-1251", "utf-8", html)
  	data := strings.Split(html, "<br/>")
	if 2 > len(data) {
		return info, errors.New("Bargain html has error.")
	}
  	info["is_done"] = strconv.FormatBool(strings.Contains(data[0], "состоялся"))
  	if "false" == info["is_done"]{
  		return info, nil
  	}
  	title_date := strings.Split(data[0], "состоялся");
  	if 2 != len(title_date) {
		return info, errors.New("Bargain title date has error.")
	}
	info["title"]  = strings.Trim(title_date[0], " \r\n\t")
	info["number"] = number
	if false == strings.Contains(data[1], "Москва") {
		if number == "26" || number == "27" || number == "28" {
			info["city"]    = "Москва"
			info["country"] = "Российская Федерация"
		} else {
			return info, errors.New("City was not determined.")	
		}
	} else {
		info["city"]    = "Москва"
		info["country"] = "Российская Федерация"
	}
	src, exists :=  doc.Find("#au-info img").Attr("src")
  	if false == exists || "" == src {
  		return info, errors.New("Bargain img not found.")
  	}
  	info["img"] = "http://www.numismat.ru" + src
  	date, err := getDate(strings.Replace(strings.Trim(title_date[1], " \r\n\t"), "  ", " ", -1))
  	if nil != err {
  		return info, errors.New("Bargain date was not defined. " + err.Error())
  	}
  	info["date_start"] = date
  	info["date_end"]   = date

  	return info, nil
}
func getMonetsAndMedalsLots(number string) ([15][]Lot, error) {
	var c chan string = make(chan string)
	var lots [15][]Lot
	periods := [15]int{130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270}
	for i := 0; i < 15; i++ {
		go downloadMonetsAndMedalsLotsThread(number, i, periods[i], c, &lots[i], i + 1)
	}
	var result [15]string
	for answer := 0;answer < 15;answer++{
		msg := <- c
		result[answer] = msg
	}
	for ri := 0; ri < 15; ri++ {
		if "End" != result[ri] {
			return lots, errors.New(result[ri])
		}
	}
	return lots, nil
	// check result
}

func downloadMonetsAndMedalsLotsThread(number string, i int, period int, c chan <- string, list *[]Lot, period_id int)  {
	// скачать русские монеты
	var page int
	var url string
	var res []Lot
	var empty bool
	var lottypes [2]string

	lottypes[0] = "30"
	lottypes[1] = "35"

	for lti := 0; lti < 2; lti++ {
		page = 1
		for ; page > 0; {
			url = "http://www.numismat.ru/au.shtml?au=" + number + "&per=" + strconv.Itoa(period) + "&lottype=" + lottypes[lti] + "&num=44&page=" + strconv.Itoa(page)
			doc, err := getHtml(url)
			if nil != err {
				c <- "MonetsAndMedalsError: Download  Lots : " + url
				return
			}
			// обработать
			empty = true
			doc.Find("#zapisi .zapis").Each(func(i int, s *goquery.Selection) {
				empty = false
				lot := new(Lot)
				lot.error  = ""
				lot.period = period_id

				// set Description
				ret := s.Find(".lot_txt p").First().Text()
				if nil != err || "" == ret {
					return
				}
				ret = convert("windows-1251", "utf-8", ret)
				lot.description = ret
				if 270 == period && -1 == strings.Index(ret, "1918") {
					return
				}
				// Set Bitkin
				if 1 < strings.Count(ret, "Биткин") {
					return
				}
				bitkin_index := strings.Index(ret, "Биткин")
				var bitkin string
				if -1 == bitkin_index {
					bs := strings.Index(ret, "Бит. ")
					if -1 == bs {
						return
					}
					bitkin = ret[(bs + 8):len(ret)]
					be := strings.Index(bitkin, " ")
					if -1 == be {
						be = len(bitkin)
					}
					bitkin = bitkin[0:be]

					if 4 < len(bitkin) {
						logger.Info("Bitkin error from Бит. - " + ret, LOG_LEVEL)
						return
					}
				} else {
					bitkin = ret[bitkin_index:len(ret)]
					bpi := strings.Index(bitkin, ".")
					if -1 == bpi {
						bpi = len(bitkin)
					}
					bitkin = bitkin[0:bpi]
					b_i := strings.Index(bitkin, " ")
					if -1 == b_i {
						b_i = len(bitkin)
					}
					ibe := strings.Index(bitkin, "#")
					if -1 == ibe {
						ibe = 0
					}
					bitkin = bitkin[:b_i]
				}
				bitkin = regexpReplace(`[^0-9]+`, "", bitkin)
				if "" == bitkin {
					return 
				}

				lot.bitkin = bitkin
				// set Title
				i_point := strings.Index(ret, ".")
				i_comma := strings.Index(ret, ",")
				if i_comma < i_point && -1 != i_comma{
					lot.title = ret[0:i_comma]
				} else {
					if -1 != i_point {
						lot.title = ret[0:i_point]
					} else {
						lot.error += "Title Error.\n"
					}
				}

				if -1 != strings.Index(lot.title, "Лот") {
					return
				}

				// Set Safety
				s_i := strings.Index(ret, "Сохранность")
				if -1 != s_i {
					safety_text := ret[s_i:len(ret)]
					s_i = strings.Index(safety_text, ".")
					if -1 == s_i {
						logger.Error(errors.New(ret), LOG_LEVEL)
						s_i = len(safety_text)
					}
					safety_text = safety_text[0:s_i] + "."
					s_i = strings.Index(safety_text, ",")
					if -1 != s_i {
						safety_text = safety_text[0:s_i]
					}
					lot.safety_text = safety_text
				}

				// Set Price
				ret, err = s.Find(".shop-priceN p").Html()
				if nil != err{
					lot.error += "Price Error.\n"
				} else {
					ret = regexpReplace(`<b>[^<]+</b>`, "", convert("windows-1251", "utf-8", ret))
					prices := strings.Split(ret, "<br/>")
					if 3 != len(prices) {
						lot.error += "Price Error.\n"
					} else {
						start_currency, start_price := getPrice(strings.Trim(prices[0], " \r\n\t"))
						currency, price := getPrice(strings.Trim(prices[1], " \r\n\t"))
						if "" != currency && currency != start_currency {
							lot.error += "Price Error.\n"
						} else {
							lot.start_price = start_price
							lot.price = price
							lot.currency = start_currency
						}
					}
				}

				// Set Front Img
				f_img := s.Find(".shop-pic img").First()
				front_img, f_exist := f_img.Attr("src")
				if false == f_exist {
					lot.error += "Front Img Error.\n"
				} else {
					lot.front_img = "http://www.numismat.ru" + front_img 
				}
				// Set Back Img
				b_img := s.Find(".shop-pic img").Last()
				back_img, b_exist := b_img.Attr("src")
				if false == b_exist {
					lot.error += "Back Img Error.\n"
				} else {
					lot.back_img = "http://www.numismat.ru" + back_img
				}

				res = append(res, *lot)
			})
			//fmt.Printf("%+v\n", res)
			if true == empty {
				page = 0
			} else {
				page++
			}
		}
	}
	*list = res
	c <- "End"
	return
}
///////////////////////////////////////////////////////////////////////////////////////


func getPrice(price string) (string, string){
	if 0 < len(price) && "$" == price[0:1] {
		return "usd", price[1:len(price)]
	}
	if -1 != strings.Index(price, "price") {
		return "rur", regexpReplace("[^0-9 –]+", "", price)
	}
	return "", ""
}

func convert(from string, to string, str string) string {
	converter, _ := iconv.NewConverter(from, to)
	str, _ = converter.ConvertString(str)
	return str
}
func regexpReplace(reg string, s string, str string) string {
	var re = regexp.MustCompile(reg)
	return re.ReplaceAllString(str, s)
}