package main

import (
    //"fmt"
    "logger"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    "models"
    "strconv"
    AGrubber "auctions/grubber"
)

func main() {
    logger.Info("Opening db connection", 0)
    //db, err := gorm.Open("postgres", "host=127.0.0.1 user=coins dbname=coins password=1111")
    db, err := gorm.Open("postgres", "host=127.0.0.1 user=coins dbname=coins password=ikj23Okd8Vji9klse3oi")
    if err != nil {
        logger.ErrorMessage("Failed to connect database", 0)
    }

    // get all auctions
    // db.LogMode(true)
    logger.Info("Find auctions list", 0)
    var auctions []models.Auction
    if err := db.Find(&auctions).Error; err != nil {
        logger.Error(err, 0)
    }

    for _, auction := range auctions {
        logger.Info("Grub auction #" + strconv.Itoa(auction.Id) + " - " + auction.Title, 1);
        gr := AGrubber.Grubber{Auction: auction, DB: db}
        gr.Grub()
    }
}